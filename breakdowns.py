#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium.webdriver.support.ui import Select
import vision
import pytest
import time
from fixtures import *

#%%

def test_ip_version_breakdown_vis(app_vis):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text("IP Version")
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")

    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    lines = vis.overview_lines
    assert lines["IPv4"] == "297 Mb"
    assert lines["IPv6"] == "1 Mb"
    assert lines["Non-IP traffic"] == "10 Mb"
    assert len(lines) == 3, "There should only be IPv4, IPv6 and non-IP traffic in the test traffic."


@pytest.mark.parametrize("breakdown_type", [
    "Application",
    "Data Source",
    "IP Protocol",
    "IP Version",
    "VLAN",
    "MPLS",
    "Source Port",
    "Destination Port",
    "Source IP Address",
    "Destination IP Address",
    "Source MAC",
    "Destination MAC"])
def test_no_null_in_breakdown(app_vis, breakdown_type):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text(breakdown_type)
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    assert "null" not in vis.overview_lines


def test_app_breakdown_vis(app_vis):
    """
    Verification query:

        WITH agg AS (
          SELECT
            application_id,
            sum(byte_count_1+byte_count_2)/1000000000*8 AS "sum"
          FROM long_flow_samples_600
          WHERE application_id NOT IN (216,469,484,503,522)
          GROUP BY application_id)
        SELECT * FROM agg NATURAL JOIN applications
        ORDER BY 2 DESC;

    Prior runnig the query, import application list into the target DB:

        pg_dump  -t applications probedb > applications.sql
        psql ... < applications.sql
    """

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text("Application")
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    line = vis.get_traffic_overview_line(1)
    assert line[0] == "Unknown", "the 1st entry should be 'Unknown' not '%s'" % line[0]

    lines = vis.overview_lines

    assert lines["Unknown"] == "211 Mb"
    assert lines["google-docs"] == "45 Mb"
    assert lines["appnexus"] == "9 Mb"
    assert lines["arp"] == "6 Mb"
    assert lines["ssdp"] == "5 Mb"
    assert lines["facebook"] == "5 Mb"
    assert lines["eth"] == "4 Mb"
    assert lines["skype"] == "4 Mb"
    assert lines["netbios_name_service"] == "4 Mb"
    assert lines["google"] == "2 Mb"
    assert lines["Other"] == "13 Mb"


def test_result_limit(app_vis):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text("Destination Port")

    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("5")

    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN

    assert len(vis.overview_lines) == 6, "Overview should have 6 entries in total"  # 5 + 1 (Other)


def test_sport_breakdown_vis(app_vis):

    """
    Query for the verification:

        SELECT
          CASE p WHEN 1 THEN port_1 ELSE port_2 END AS "port",
          sum(CASE p WHEN 1 THEN byte_count_1 ELSE byte_count_2 END)/1000000000*8
        FROM long_flow_samples_600, (VALUES (1),(2)) AS ep(p)
        WHERE application_id NOT IN (216,469,484,503,522)
        GROUP BY CASE p WHEN 1 THEN port_1 ELSE port_2 END
        ORDER BY 2 DESC
        LIMIT 10;
    """

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text("Source Port")
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN

    lines = vis.overview_lines
    
    assert lines.get("TCP/22: ssh") == "103 Mb"
    assert lines.get("TCP/443: https") == "94 Mb"
    assert lines.get("TCP/80: http") == "27 Mb"
    assert lines.get("Non-IP traffic") == "10 Mb"
    assert lines.get("TCP/35874") == "8 Mb"
    assert lines.get("TCP/51581") == "6 Mb"
    assert lines.get("TCP/39806") == "4 Mb"
    assert lines.get("UDP/137: netbios-ns") == "4 Mb"
    assert lines.get("TCP/34709") == "4 Mb"
    assert lines.get("TCP/54125") == "3 Mb"
    assert lines.get("Other") == "46 Mb"


def test_dport_breakdown_vis(app_vis):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text("Destination Port")
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN

    lines = vis.overview_lines
    
    assert lines.get("TCP/42599") == "76 Mb"
    assert lines.get("TCP/49365") == "43 Mb"
    assert lines.get("TCP/35874") == "25 Mb"
    assert lines.get("TCP/34709") == "25 Mb"
    assert lines.get("TCP/443: https") == "23 Mb"
    assert lines.get("TCP/80: http") == "22 Mb"
    assert lines.get("TCP/54290") == "13 Mb"
    assert lines.get("TCP/22: ssh") == "11 Mb"
    assert lines.get("Non-IP traffic") == "10 Mb"
    assert lines.get("TCP/36935") == "5 Mb"
    assert lines.get("Other") == "55 Mb"


def test_data_source_breakdown_vis(app_vis, rotfile=default_rotfile):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")).select_by_visible_text("Data Source")
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN

    line = vis.get_traffic_overview_line(1)
    assert rotfile in line[0], "the source should include %s" % rotfile
    assert line[1] == "309 Mb"

    lines = vis.overview_lines
    assert len(lines) == 1, "There should only be one data source synthetic_telstra_nexus: lines= %s" % lines


def test_protocol_traffic_over_time(app_vis):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    # need to restart DBMGR to flush all uview temporal tables
    cli = vision.Cli(vis.probe)
    cli.restart_service("dbmgr")
    cli.close()


    #vis.get_vis(name)
    view = vis.view_id
    #v.query_timerange = (1391706363+10,1391706363+20)


    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    Select(driver.find_element_by_id("select_breakdown_types")).select_by_visible_text("IP Protocol")
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN_OVER_TIME
    start, _ = vis.data_time_range
    vis.query_time_range = (start + 9, start + 15)

    assert driver.find_elements_by_css_selector("g > g > path"), "Hightchart char should be drawn"

    svg_paths = driver.find_elements_by_xpath("//*[local-name() = 'path' and namespace-uri()='http://www.w3.org/2000/svg' and @fill='none']")
    len(svg_paths) == 31 

    lines = vis.overview_lines
    assert len(lines) == 3
    assert "TCP" in lines

    # verify that uView table got created with correct prarameters:
    db = vision.MetaDb(probe=vis.probe, rotfile=vis.rotfile)
    rows = db.query_all("SELECT * FROM uviews")
    assert len(rows) == 1

    ts_from, ts_to = rows[0][3:5]
    #assert ts_from == (start + 13)<<32
    #assert ts_to == (start + 19)<<32


@pytest.mark.parametrize("breakdown_type", [
    "Application",
    "Data Source",
    "IP Protocol",
    "IP Version",
    "VLAN",
    "MPLS",
    "Source Port",
    "Destination Port",
    "Source IP Address",
    "Destination IP Address",
    "Source MAC",
    "Destination MAC"])
def test_uview_breakdown_ot(uview_ot_vis, breakdown_type):

    vis = uview_ot_vis
    driver = vis.driver

    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text(breakdown_type)
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN_OVER_TIME

    start, _ = vis.data_time_range
    vis.query_time_range = (start + 19, start + 19.303)

    assert driver.find_elements_by_css_selector("g > g > path"), "Hightchart char should be drawn"
    # TODO: some more condition tests

#%%
if __name__ == "__main__":
    # user_traffic = os.environ.get("TRACEFILE")
    # if user_traffic is None: user_traffic = default_tracefile
    capture_traffic()
