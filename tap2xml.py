# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 16:26:11 2014

@author: rad.cirskis
"""

import vision
from collections import deque
import sys
import re
from xml.sax.saxutils import escape

lines = None
# current line
line = ""

cases = dict()
failures_total = 0
total_tests = 0
total_failures = 0

test_line_re = re.compile("^(ok|not ok)\s*([0-9]+)(?: - )?(.*)$", re.M)

def read_line():
    global line, tap_input, lines

    max_empty_lines = 30

    while True:
        try:
            line = lines.popleft()
        except:
            return None

        #line = tap_input.readline()

        if line is None or line.strip() != "":
            break

        if max_empty_lines == 0:
            line = None
            break

        max_empty_lines -= 1


def parse_test_case():
    """
    attempts to find a line like
    '# ut.test_flow_volume_multiple_flows_disordered()'
    """
    global line, failures_total, total_tests, total_failures
    test_case = "Default"
    tests = []

    while line is not None:
        if line.startswith('# ') or " ok " in line:
            if line.startswith("# Looks like you failed "):
                total_tests, total_failures = re.search("# Looks like you failed (\d*) tests? of (\d*)", line).groups()
                return None
            if line.startswith('# '):
                test_case = line[2:].strip().replace('"', '')
                read_line()
            while line is not None and (line.startswith("ok ") or line.startswith("not ok ")):
                failures = []
                res, num, message = test_line_re.search(line).groups()
                if res == "not ok":
                    failures = [line.split("\n",1)[-1]]
                tests.append((res == "ok", num, message, failures))
                read_line()

            return (test_case, tests)
        read_line()
    return None

def dump_xml(cases):

    print """<?xml version="1.0" encoding="UTF-8" ?>"""
    print """<testsuites failures="%d">""" % failures_total
    for c in cases:
        print """<testsuite name="%s">""" % c
        for t in cases[c]:
            ok, num, message, failures = t
            message = escape(message)
            print """<testcase classname="%s" name="%s" assertions="%s">""" % (c, num, message.replace('"','&quot;'))
            if not ok:
                print """<failure message="%s" type="failure">""" % message
                for l in failures:
                    print escape(l)
                print "</failure>"
            print "</testcase>"
        print "</testsuite>"
    print "</testsuites>"

def main():
    """
    Parses TAP file and generates jUnit XML output
    """

    global cases, failures_total, tap_input, lines

    tap_input = sys.stdin
    mdb = vision.MetaDb()

    if len(sys.argv) < 2:
        db_name = 'probedb'
    else:
        db_name = sys.argv[1]

    lines = deque(mdb.run_pgtap_tests(mdb.probedb if db_name == 'probedb' else mdb.erfstoredb))

    try:
        while True:

            tc = parse_test_case()
            if tc is None:
                break
            cases[tc[0]] = cases.get(tc[0], []) + tc[1]
    except Exception as e:
        print "*** Last line: %s" % line
        raise e

    dump_xml(cases)

if __name__ == "__main__":
    main()
