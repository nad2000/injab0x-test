#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Reporting testing:

 - virtualevn ~/testenv
 - . ~/testenv/bin/activate
 - pip install -U -r requirements.txt
 - py.test -s reporting.py

"""

from selenium.webdriver.support.ui import Select
import vision
import os
import pytest
import re
import time
from fixtures import *

#
# Prameters:
# startign timestamp or 0 (the test will start with the smallest available value for the view)
# eg, datetime.datetime.strptime('Wed, 7 Aug 2013 06:54:00', '%a, %d %b %Y %H:%M:%S').strftime("%s")

@pytest.mark.parametrize("report_template", [
    'bandwidth_a4',
    'bandwidth_letter',
    'conversations_ip_and_port_detail_a4',
    'conversations_ip_and_port_detail_letter',
    'conversations_ip_and_port_summary_a4',
    'conversations_ip_and_port_summary_letter',
    'conversations_ip_detail_a4',
    'conversations_ip_detail_letter',
    'conversations_ip_summary_a4',
    'conversations_ip_summary_letter',
    'conversations_mac_detail_a4',
    'conversations_mac_detail_letter',
    'conversations_mac_summary_a4',
    'conversations_mac_summary_letter',
    'top_talkers_ip_a4',
    'top_talkers_ip_letter',
    'top_talkers_mac_a4',
    'top_talkers_mac_letter',
    'traffic_breakdown_app_a4',
    'traffic_breakdown_app_letter',
    'traffic_breakdown_dst_ip_a4',
    'traffic_breakdown_dst_ip_letter',
    'traffic_breakdown_dst_mac_a4',
    'traffic_breakdown_dst_mac_letter',
    'traffic_breakdown_dst_port_a4',
    'traffic_breakdown_dst_port_letter',
    'traffic_breakdown_mpls_a4',
    'traffic_breakdown_mpls_letter',
    'traffic_breakdown_over_time_app_a4',
    'traffic_breakdown_over_time_app_letter',
    'traffic_breakdown_over_time_dst_ip_a4',
    'traffic_breakdown_over_time_dst_ip_letter',
    'traffic_breakdown_over_time_dst_mac_a4',
    'traffic_breakdown_over_time_dst_mac_letter',
    'traffic_breakdown_over_time_dst_port_a4',
    'traffic_breakdown_over_time_dst_port_letter',
    'traffic_breakdown_over_time_mpls_a4',
    'traffic_breakdown_over_time_mpls_letter',
    'traffic_breakdown_over_time_proto_a4',
    'traffic_breakdown_over_time_proto_letter',
    'traffic_breakdown_over_time_src_ip_a4',
    'traffic_breakdown_over_time_src_ip_letter',
    'traffic_breakdown_over_time_src_mac_a4',
    'traffic_breakdown_over_time_src_mac_letter',
    'traffic_breakdown_over_time_src_port_a4',
    'traffic_breakdown_over_time_src_port_letter',
    'traffic_breakdown_over_time_vlan_a4',
    'traffic_breakdown_over_time_vlan_letter',
    'traffic_breakdown_proto_a4',
    'traffic_breakdown_proto_letter',
    'traffic_breakdown_src_ip_a4',
    'traffic_breakdown_src_ip_letter',
    'traffic_breakdown_src_mac_a4',
    'traffic_breakdown_src_mac_letter',
    'traffic_breakdown_src_port_a4',
    'traffic_breakdown_src_port_letter',
    'traffic_breakdown_vlan_a4',
    'traffic_breakdown_vlan_letter'])
def test_reports_can_be_executed(capture_traffic, report_template):
    """
    Simple reporting sanity test: all reports can be run successfully.
    """

    global default_rotfile
    rotfile = default_rotfile
    cli = capture_traffic

    cli.execute("show erfstream erfstore name %s" % rotfile)
    ts_start, ts_end = re.findall("(?:Start|End) Time.*([0-9]{4}/[/ 0-9:].*)\r", cli.ssh.before, re.M)

    cmd_prefix = "vision report job %s " % report_template
    cli.execute(cmd_prefix + "template %s" % report_template)
    cli.execute(cmd_prefix + "output filename %s" % report_template)
    cli.execute(cmd_prefix + "rotation-file %s appliance localhost" % rotfile)
    cli.execute(cmd_prefix + "time-start %s" % ts_start)
    cli.execute(cmd_prefix + "time-end %s" % ts_end)

    cli.execute("vision report run job %s" % report_template)

    max_attempts = 12
    #cli.execute("quit")

    time.sleep(10)
    for _ in range(max_attempts):
        time.sleep(5)
        #cli.execute("ls /endace/vision/reports/output/%s*" % report_template)
        cli.execute("show vision report files")
        output = cli.ssh.before
        if report_template in output:
            break

    #cli.execute("cli -m config")

    assert report_template in output
