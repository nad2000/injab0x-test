#!/usr/bin/env python
#from selenium import webdriver
#from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
#import selenium.webdriver.support.ui as ui
#from selenium.common.exceptions import NoSuchElementException
import pytest
import time
import re
import random
import vision
from vision import ts2string
import sys
import os

#
# Prameters:
probe = os.environ.get("TARGET_HOST", "192.168.132.111")
# startign timestamp or 0 (the test will start with the smallest available value for the view)
# eg, datetime.datetime.strptime('Wed, 7 Aug 2013 06:54:00', '%a, %d %b %Y %H:%M:%S').strftime("%s")
ts_start = int(os.environ.get("TS_START", 0))  # or 0
ts_step = int(os.environ.get("TS_STEP", 60*15))   # 15 min
ts_gap = int(os.environ.get("TS_GAP", 60*30))    # 30 min

rotfile = os.environ.get("ROTFILE", 'synthetic_telstra_nexus')
default_tracefile = rotfile + '.erf'
#

stats = []

def create_vis(
        probe=probe,
        name=None,
        rotfile=rotfile,
        tracefile=default_tracefile,
        replay=True):
    """
    Create application breakdown with 4 top applications filtered-out

    name - name of the vision view
    rotfile - view over this rotfile (captured traffic)
    replay - capture with "replay" (for uView testing, you can replay=False)
    """

    if name is None:
        name = rotfile + "_vis"

    # we need to restart DBMGR because otherwise the uviews from previous run are reused
    #cli = vision.Cli(probe=probe)
    #cli.restart_service("dbmgr")
    #time.sleep(5)
    #cli.save_config()
    #cli.close()

    #capture_traffic(rotfile=rotfile, clean=False)
    vis = vision.Vision(probe=probe)

    vis.delete_visualization(name)

    vis.get_vision()
    driver = vis.driver

    driver.find_element_by_id("add_vis_link").click()
    driver.find_element_by_id("add_vis_name").clear()
    driver.find_element_by_id("add_vis_name").send_keys(name)

    Select(driver.find_element_by_name("add_vis_ids")
           ).select_by_visible_text(rotfile)
    driver.find_element_by_id("vis_type_2").click()
    driver.find_element_by_id("form_submit").click()
    vis.wait_until_complete()

    view = vis.view_id

    # Change to 'Source IP Address':
    #Select(driver.find_element_by_id("select_breakdown_types")
    #       ).select_by_visible_text("Source IP Address")
    #driver.find_element_by_id("breakdown_num_results").clear()
    #driver.find_element_by_id("breakdown_num_results").send_keys("10")

    #driver.find_element_by_id("filters_apply").click()
    #vis.wait_until_complete()

    driver.find_element_by_link_text("Add filter").click()
    Select(driver.find_element_by_id("add_filter_by")).select_by_visible_text("Destination IP Address")
    driver.find_element_by_id("add_filter_submit").click()

    driver.find_element_by_id("token-input-filter_value_i_dip").click()
    driver.find_element_by_id("token-input-filter_value_i_dip").send_keys("192.168.1.200" + Keys.ENTER)
    driver.find_element_by_css_selector("input.update_button.update_row_button").click()
    vis.wait_until_complete()

    # Filter in a single IP address:
    #vis.check_select("192.168.1.200")
    #vis.check_select("udp")
    #vis.check_select("ssl")
    #vis.check_select("tcp")
    #vis.check_select("smtp")

    

    #try:
    #    # Filter out selected:
    #    driver.find_element_by_id("filter_selected_i").click()
    #    vis.wait_until_complete()
    #except:
    #    pass

    # Reduce time range to one min:
    driver.find_element_by_id("vis_time_1min_" + view).click()
    vis.wait_until_complete()

    # Change to 'Source Port':
    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text("Source Port")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    return vis


@pytest.fixture(scope="session")
def app_vis():
    """
    Create application breakdown with 4 top applications filtered-out
    """

    vis = create_vis()

    def fin():
        print "### Finalizing ###"
        try:
            vis.close_driver()
        except:
            pass

    return vis


# Change time range and update visualization:
def query_at(vis, ts):

    driver = vis.driver
    view = vis.view_id
    
    driver.find_element_by_id("vis_time_link_" + view).click()
    driver.find_element_by_id("pop_time_from_" + view).click()
    driver.find_element_by_id("pop_time_from_" + view).clear()
    driver.find_element_by_id(
        "pop_time_from_" + view).send_keys(ts2string(ts))
    driver.find_element_by_id("pop_time_to_" + view).click()
    driver.find_element_by_id("pop_time_to_" + view).clear()
    driver.find_element_by_id(
        "pop_time_to_" + view).send_keys(ts2string(ts + ts_step))

    t0 = time.time()
    driver.find_element_by_id("selection_button_" + view).click()
    vis.wait_until_complete()
    ts_delta = (time.time() - t0)
    print "%d,%d,%0.2f" % (int(round(time.time())), ts, ts_delta)
    stats.append((ts, ts_delta))
    

def test_performance(app_vis):

    vis = app_vis
    driver = vis.driver

    global ts_start, probe, rotfile, ts_gap

    #self.rotfile_exists(rotfile)
    vis_name = rotfile + '_vis'
    #vis = create_app_vis(vis_name, rotfile)

    vis.get_vis(vis_name)

    db = vision.MetaDb(probe=probe, rotfile=rotfile)
    ts_min_s = int(db.last_start_ts)
    
    # find the stores margines (max and min timestamp values)
    #ts_min_s = max(
    #    int(re.search(
    #        "var ts_min_s\t= ([0-9]+);", driver.page_source).group(1)),
    #    self.last_start_ts)

    ts_max_s = int(
        re.search("var ts_max_s\t= ([0-9]+);", driver.page_source).group(1))
    if ts_start == 0 or ts_start >= ts_max_s:
        ts_start = ts_min_s

    # firts TS slightly randomly offset
    ts = ts_start + random.randint(1, ts_step + 1)

    # iterate:

    points = range(ts, ts_max_s - ts_step, ts_step + ts_gap)

    for ts in points:
        query_at(vis, ts)


if __name__ == "__main__":
    if len(sys.argv) > 1 and not sys.argv[1].startswith("--"):
        probe = sys.argv[1]
        del sys.argv[1]
    if len(sys.argv) > 1 and not sys.argv[1].startswith("--"):
        rotfile = sys.argv[1]
        del sys.argv[1]

    vis = create_app_vis()
    test_performance(vis)
