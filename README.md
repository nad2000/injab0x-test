# injab0x tests

## USAGE:

1. Setup Python virtual environment;
1. Run the tests;

## Virtual evnvironment for running tests:

```
cd injab0x-test
virtualenv --distribute ../testevn
source ../testevn/bin/activate
pip install -U -r requirements.txt
```

## How to run the tests

```
export TARGET_HOST=fin7000-3        ## set up the test probe (default: 192.168.132.111)

# Optionally (if you run the test remotely, e.g., on appbuild-vm):
export HUB_ADDRESS=nzaklwks0084     ## set up Selenium server (default: nzaklwks0084)
epxort HUB_PORT=4444                ## Selenium hub port (default: 4444)

cd injab0x-test
source ../testevn/bin/activate
py.test ./cpd.py                    ## to run a test suite
py.test ./                          ## to discover and run all tests in the directory
py.test --junit-xml cpd.xml cpd.py  ## to generate jUnit XML report (for Bamboo)
```

 NB! Make sure your **.ssh/known_hosts** doesn't have stale keys. Otherwise the
tests with CLI will fail.

