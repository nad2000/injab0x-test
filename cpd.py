#!/usr/bin/env python
# -*- coding: utf-8 -*-

from fixtures import *
slow = pytest.mark.slow

#import unittest

default_probe = vision.default_probe
default_rotfile = "cpd_test"
default_tracefile = "synthetic_telstra_nexus.erf"
default_netsource_port = 7777

#@pytest.fixture(scope="session")
def capture_traffic(probe=default_probe,rotfile=default_rotfile, replay=True, traffic=default_tracefile):

    """
    Creates a dag to rotfile pipe,
    runs dagflood and captures.
    """

    cli = vision.Cli(probe=probe, user="root", verbose=True)
    cli.capture_traffic(rotfile=rotfile, replay=replay, clean=False, traffic=traffic,
                        interface='dagmod.3' if probe=='192.168.132.139' else None,
                        device=0 if probe=='192.168.132.139' else None)
    return cli



#%%


def capture_packets(
    probe=default_probe, rotfile=default_rotfile, packets=test_packets,
    repeat=100, real_time=False,
    start_ts=None, delta=1):

    """
    Captures packets from the list into specified rotfile on the probe.
    Repeately runs the list of packets.
    if real_time is False, start time will current time - 1000 sec and erf TS will be set
    so that one packet will captured per sec.
    """

    pipe = rotfile+'_pipe'
    port = default_netsource_port

    cli = vision.Cli(probe=probe, verbose=True)

    # clean-up after previous runs:
    cli.stop_pipe(pipe)
    cli.delete_pipe(pipe)
    cli.delete_rotfile(rotfile)
    cli.delete_netsource(port=port)

    # set-up test:
    ns_name = 'p' + str(port)
    cli.create_rotfile(name=rotfile)
    cli.create_netsource(port=port, name=ns_name)
    cli.create_netsource_pipe(name=pipe, rotfile=rotfile, netsource=ns_name, app_detect=False)

    cli.start_pipe(pipe)

    ns = vision.NetworkSource(probe=probe,port=port)

    if real_time:

        for _ in xrange(repeat):
            for p in packets:
                ns.write(p)

    else:

        ts = time.time() - 1000 if start_ts is None else start_ts

        for _ in xrange(repeat):
            for p in packets:
                ts = ns.write(p, ts=ts) + delta

    ns.close()

    cli.stop_pipe(pipe)


def test_dagflood(capture_traffic_from_dag, probe=default_probe, rotfile="synthetic_telstra_nexus"):
    cli = capture_traffic_from_dag

    db = vision.MetaDb(probe=probe,rotfile=rotfile)
    assert db.source_exists(rotfile), "Erfstore "+rotfile+"should be created."


def test_multiple_flows():

    probe = default_probe
    rotfile = "test_multiple_flows"

    repeat = 102030
    capture_packets(rotfile=rotfile, real_time=False,
                  repeat=repeat, start_ts=time.time()-repeat, delta=0.1)

    db = vision.MetaDb(probe=probe,rotfile=rotfile)
    packet_count, byte_count = db.get_counts()

#  packet_diffs = db.query_all("""WITH diffs AS (
#  SELECT packet_count-(packet_count_1+packet_count_2) AS diff_pc
#  FROM bandwidth_stats JOIN long_flow_samples
#    ON last_time_sec = timestamp)
#  SELECT * FROM diffs WHERE diff_pc != 0""")

#  byte_diffs = db.query_all("""WITH diffs AS (
#  SELECT packet_count-(packet_count_1+packet_count_2) AS diff_bc
#  FROM bandwidth_stats JOIN long_flow_samples
#    ON last_time_sec = timestamp)
#  SELECT * FROM diffs WHERE diff_bc != 0""")

    # Packet count doesn't match:
    packets_sent = repeat * len(test_packets)
    assert packet_count == packets_sent, "Packet counts (sent and stored in the meta DB) should match: %d, %d" % \
        (packet_count, packets_sent)

    # Byte count doesn't match:
    bytes_sent = sum(repeat * len(r) for r in test_packets)
    assert byte_count == bytes_sent, "Byte counts (sent and stored in the meta DB) should match: %d, %d" % \
        (byte_count, bytes_sent)

    # Test for http://jira.ems.endace.com:8080/browse/EP-454
    #assert len(byte_diffs) == 0, "Byte count should match between bandwidth stats and samples"
    #assert len(packet_diffs) == 0, "Packet count should match between bandwidth stats and samples"


def test_sinlge_flow():

    probe = default_probe
    rotfile = "test_sinlge_flow"
    pipe = rotfile + "_pipe"

    cli = vision.Cli(probe=probe)

    # clean-up after previous runs:
    cli.stop_pipe(pipe)
    cli.delete_pipe(pipe)
    cli.delete_rotfile(rotfile)
    cli.delete_netsource()

    # set-up test:
    cli.create_rotfile(rotfile)
    cli.create_netsource()
    cli.create_netsource_pipe(name=pipe, rotfile=rotfile)

    cli.start_pipe(pipe)

    p1 = Ether(src='2c:76:8a:d9:dc:8b', dst='01:00:5e:7f:ff:fa', type=2048) \
        / IP(frag=0L, src='192.168.129.35', proto=17, tos=0, dst='239.255.255.250', chksum=10047, len=474, options=[], version=4L, flags=0L, ihl=5L, ttl=1, id=24334)/UDP(dport=1900, sport=1900, len=454, chksum=9378)/Raw(load='NOTIFY * HTTP/1.1\r\nHost:239.255.255.250:1900\r\nNT:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nNTS:ssdp:alive\r\nLocation:http://192.168.129.35:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n')
    p2 = Ether(dst='2c:76:8a:d9:dc:8b', src='01:00:5e:7f:ff:fa', type=2048) \
        / IP(frag=0L, dst='192.168.129.35', proto=17, tos=0, src='239.255.255.250', chksum=10047, len=474, options=[], version=4L, flags=0L, ihl=5L, ttl=1, id=24334) \
        / UDP(sport=1900, dport=1900, len=454, chksum=9378) \
        / Raw(load='NOTIFY * HTTP/1.1\r\nHost:239.255.255.250:1900\r\nNT:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nNTS:ssdp:alive\r\nLocation:http://192.168.129.35:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n')

    # Packet byte-strings:
    rr1, rr2 = str(p1), str(p2)

    ns = vision.NetworkSource(probe=probe)
    ts = time.time()-600
    for _ in xrange(100):
        ts = ns.write(rr1, ts=ts + 1)
        ts = ns.write(rr2, ts=ts + 1)
    ns.close()
    cli.stop_pipe(pipe)
    cli.close()

    db = vision.MetaDb(probe=probe,rotfile=rotfile)
    packet_count, byte_count = db.get_counts()

#    packet_diffs = db.query_all("""WITH diffs AS (
#    SELECT packet_count-(packet_count_1+packet_count_2) AS diff_pc
#    FROM bandwidth_stats JOIN long_flow_samples
#      ON last_time_sec = timestamp)
#    SELECT * FROM diffs WHERE diff_pc != 0""")

#    byte_diffs = db.query_all("""WITH diffs AS (
#    SELECT packet_count-(packet_count_1+packet_count_2) AS diff_bc
#    FROM bandwidth_stats JOIN long_flow_samples
#      ON last_time_sec = timestamp)
#    SELECT * FROM diffs WHERE diff_bc != 0""")

    # Packet count doesn't match:
    assert packet_count == 100*2, "Packet counts (sent and stored in the meta DB) should match"
    # Byte count doesn't match:
    assert byte_count == 100*len(rr1) + 100*len(rr2), "Byte counts (sent and stored in the meta DB) should match"

    # Test for http://jira.ems.endace.com:8080/browse/EP-454
    ##self.assertEqual(len(byte_diffs), 0, "Byte count should match between bandwidth stats and samples")
    ##self.assertEqual(len(packet_diffs), 0, "Packet count should match between bandwidth stats and samples")


def test_nic():

    probe = default_probe
    rotfile = "nic"
    pipe = rotfile + "_pipe"

    cli = vision.Cli(probe=probe)

    # clean-up after previous runs:
    cli.stop_pipe(pipe)
    cli.delete_pipe(pipe)
    cli.delete_rotfile(rotfile)
    cli.delete_netsource()

    # set-up test:
    cli.create_rotfile(rotfile, size=64)
    cli.create_nic_pipe(name=pipe, rotfile=rotfile, nic='eth0')

    cli.start_pipe(pipe)
    time.sleep(10)
    cli.stop_pipe(pipe)

    db = vision.MetaDb(probe=probe, rotfile=rotfile)
    packet_count, byte_count = db.get_counts()

    assert packet_count > 0
    assert byte_count > 0


if __name__ == "__main__":
    #buidl_data_uview()
    test_nic()
