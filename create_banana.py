#!/usr/bin/env python
import argparse
import os
import time
from random import randint

host = os.environ.get('TARGET_HOST', '192.168.132.111')
ts0 = 1423805742.0 # Fri, 13 Feb 2015 05:35:42 GMT

parser = argparse.ArgumentParser(description='Generate TCP session ERF records with preset IRTT.')
parser.add_argument('output', nargs='?', type=str, help='Outpug ERF file (default: banana.erf)', default='banana.erf')
parser.add_argument('--host', help='the host to "netcat" generated ERFs (default: %s)' % host, default=host)
parser.add_argument('-p', '--port', type=int, help='(default: NONE)')
parser.add_argument('--handshake', type=int, default=1, help='Handshake type: 1 - 3-way Handshake, 2 - Mutual Acknowledgement, 3 - Reverse Mutual Acknowledgement (default: 1)')
parser.add_argument('-V', '--validation', default=False, action='store_true', help='Generate test data for the validation testing with IRTT=((min*60+sec) mod 42 + 1) ms (default: No)')
parser.add_argument('-6', '--ipv6', default=False, action='store_true', help='Use IPv6 (default: No)')
#parser.add_argument('-M', '--mixed', default=False, action='store_true', help='Use both IPv4 and IPv6 (default: No)')
parser.add_argument('-v', '--verbose', default=False, action='store_true', help='(default: No)')
parser.add_argument('-r', '--retransmission', default=False, action='store_true', help='(default: No)')
parser.add_argument('-t', '--terminate', type=int, default=3600, help='Stop transmitting after <n> seconds (default: 1 hour)')
parser.add_argument('-S', '--summary', action='store_true', default=False, help='Summary file with generated timestamps and IRTT values (default: No)')
parser.add_argument('-s', '--start', type=float, dest='start', default=ts0,
                    help='Sart transmitting with the timestamp <n> seconds (default: %.3d = %s)' \
                    % (ts0, time.strftime('%c', time.gmtime(ts0))))
args = parser.parse_args()
ts0 = args.start
handshake = args.handshake

if args.verbose:
    print '='*80
    print args
    print '='*80

import vision
from scapy.all import Ether, IP, TCP, Raw, IPv6, Dot1Q
from itertools import product

if args.summary:
    import csv
    summary_csv = open(os.path.splitext(args.output)[0] + ".csv", "wb")
    irtt_csv = open(os.path.splitext(args.output)[0] + "_irtt.csv", "wb")
    print >>summary_csv, "ts, min_irtt, avg_irtt, max_irtt, retransmission"
    print >>irtt_csv, "ts, src, dst, irtt"

profile = [
    # ts (relative sec with fraction), min (ms), max(ms), avg (ms), TCP retransmission
    (0,  14, 37, 23,  3),
    (1,  15, 34, 23, 10),
    (2,  17, 39, 23, 12),
    (3,  18, 40, 23, 11),
    (4,  19, 35, 25, 17),
    (5,  16, 34, 23,  1),
    (6,  15, 33, 23, 11),
    (7,  11, 37, 22, 17),
    (8,  10, 36, 21, 21),
    (9,   9, 37, 20, 11),
    (10,  8, 35, 25, 13),
    (11, 11, 34, 26, 14),
    (12, 12, 35, 23, 15),
    (13, 14, 36, 22, 16),
    (14, 13, 37, 21,  8),
    (15, 14, 38, 20, 15),
    (16, 17, 39, 19, 14),
    (17, 13, 34, 20, 19),
    (18, 12, 35, 22, 18),
    (19, 11, 37, 23, 17),
    (20, 10, 38, 24, 16),
    (21,  9, 33, 20,  5),
    (22, 11, 39, 21, 14),
    (23, 12, 31, 22, 13),
    (24, 15, 33, 23, 12),
    (25, 16, 34, 24, 11),
    (26, 13, 37, 22, 10),
    (27, 12, 33, 25, 11),
    (28, 10, 32, 26,  2),
    (29, 11, 39, 27, 15),
    (31, 12, 37, 23, 17),
    (32, 12, 27, 23, 19),
    (33, 12, 37, 23, 21),
    (34, 12, 32, 23,  3),
    (35, 12, 37, 23, 24),
    (36, 12, 29, 23, 22),
    (37, 12, 32, 23, 12),
    (38, 12, 37, 23, 14),
    (39, 12, 37, 23, 15),
    (40, 12, 37, 23,  6),
    (41, 12, 33, 23, 17),
    (42, 12, 37, 23, 12),
    (43, 12, 27, 23, 11),
    (44, 12, 32, 23,  8),
    (45, 12, 37, 23,  5),
    (46, 12, 29, 23,  3),
    (47, 12, 27, 23,  1),
    (48, 12, 32, 23, 10),
    (49, 12, 32, 23, 12),
    (50, 12, 33, 23, 10),
    (51, 12, 37, 23,  6),
    (52, 12, 27, 23, 10),
    (53, 12, 39, 23, 19),
    (54, 12, 32, 23, 10),
    (55, 12, 37, 23,  2),
    (56, 12, 33, 23, 10),
    (57, 12, 32, 23, 24),
    (58, 12, 37, 23, 10),
    (59, 12, 32, 23, 13),
]

# relative latency change (per minute):
hourly_trend = [
    -0.19, -0.15, 0.23, 0.23, 0.14, 0.04, 0.1, 0.15, 0.08, -0.07, -0.2, 0.01,
    0.24, 0.22, -0.21, -0.15, 0.06, -0.07, -0.05, 0.12, 0.17, 0.03, -0.2, -0.14,
    -0.21, -0.01, 0.12, 0.06, -0.12, 0.03, 0.09, 0.22, -0.15, 0.17, 0.09, -0.04,
    0.01, 0.16, -0.18, -0.05, -0.13, 0.1, 0.01, 0.14, -0.21, 0.18, -0.11, -0.19,
    -0.02, 0.12, -0.22, -0.16, 0.02, 0.13, -0.21, 0.01, -0.22, 0.15, -0.14]

# TODO:
# (per hour)
daily_trend = [
    # 0am  1am ...                      6am
    -0.1, -0.23, -0.34, -0.32, -0.35, -0.31,
    # 7am ...                  12am
    -0.27, -0.28, -0.2, -0.1, 0, 0,
    # 1pm ...                  6pm
    0.1, 0.2, 0.21, 0.22, 0.24, 0.22,
    # 7pm ...              11pm
    0.3, 0.34, 0.29, 0.22, 0.1]

# TODO:
# monthly trend (per day):
# logarithmic: [round( (log(i+1) / 3.5 ) + (random.random()/5) - 0.6,2)  for i in xrange(0,30)]
monthly_trend = [
    -0.52, -0.24, -0.24, -0.08, -0.04, -0.08, 0.05, 0.01, 0.19, 0.14, 0.12,
    0.24, 0.22, 0.34, 0.23, 0.35, 0.39, 0.31, 0.26, 0.28, 0.28, 0.47, 0.33,
    0.5, 0.33, 0.43, 0.35, 0.38, 0.43, 0.42, 0.23, 0.11, 0.1, 0.0,]

# TCP sessions:
src_dst = [
    # source IP       destintion IP    sport, dport
    ('192.168.1.103', '192.168.1.104', 12348, 80),
    ('192.168.1.150', '192.168.1.127', 32812, 25),
    ('192.168.1.140', '192.168.1.129', 45234, 80),
    ('192.168.1.148', '192.168.1.110', 34987, 443),
    ('192.168.1.106', '192.168.1.123', 32434, 80),
    ('192.168.1.134', '192.168.1.121', 62186, 443),
    ('192.168.1.105', '192.168.1.124', 89554, 80),
    (   '172.16.1.1', '192.168.1.128', 54867, 443),
    ('192.168.1.147', '192.168.1.119', 74564, 80),
    ('192.168.1.125', '192.168.1.109', 35633, 443),
    ('192.168.1.108', '192.168.1.112', 45345, 80),
    ('192.168.1.130', '192.168.1.151', 56734, 25),
    ('192.168.1.118', '192.168.1.132', 54356, 80),
    ('192.168.1.113', '192.168.1.122', 14538, 8080),
]

def v4_to_v6(ip_address):
    numbers = list(map(int, ip_address.split('.')))
    return '2002:{0:02x}{1:02x}:{2:02x}{3:02x}::'.format(*numbers)
    

# for simplicity all MACs are the same:
smac = '00:26:f1:43:ae:00'
dmac = '10:60:4b:5a:c6:15'

# for PCAP user Pcap writer:
#pktdump = PcapWriter("banana.pcap")


if args.ipv6:
    src_dst = [(v4_to_v6(src), v4_to_v6(dst), sport, dport) for src, dst, sport, dport in src_dst]
    session = [
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::2', dst='::1') / TCP(seq=10L, flags="S", sport=80, dport=1234),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::1', dst='::2') / TCP(seq=20L, ack=11L, flags="SA", sport=1234, dport=80),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::2', dst='::1') / TCP(seq=11L, ack=21L, flags="A", sport=80, dport=1234),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::2', dst='::1') / TCP(seq=11L, ack=21L, flags="PA", sport=80, dport=1234) / Raw(load='REQUEST ' * 3),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::1', dst='::2') / TCP(seq=21L, ack=12L, flags="A", sport=1234, dport=80),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::1', dst='::2') / TCP(seq=21L, ack=11L, flags="PA", sport=1234, dport=80) / Raw(load='REPLY ' * 3),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::2', dst='::1') / TCP(seq=11L, ack=22L, flags="A", sport=80, dport=1234),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::2', dst='::1') / TCP(seq=11L, flags="F", sport=80, dport=1234),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::1', dst='::2') / TCP(seq=11L, ack=12L, flags="A", sport=1234, dport=80),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::1', dst='::2') / TCP(seq=11L, flags="F", sport=1234, dport=80),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::2', dst='::1') / TCP(seq=11L, ack=12L, flags="A", sport=80, dport=1234),
    ]

else:
    session = [
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.2', dst='10.0.0.1') / TCP(seq=10L, flags="S", sport=80, dport=1234),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.1', dst='10.0.0.2') / TCP(seq=20L, ack=11L, flags="SA", sport=1234, dport=80),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.2', dst='10.0.0.1') / TCP(seq=11L, ack=21L, flags="A", sport=80, dport=1234),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.2', dst='10.0.0.1') / TCP(seq=11L, ack=21L, flags="PA", sport=80, dport=1234) / Raw(load='REQUEST ' * 3),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.1', dst='10.0.0.2') / TCP(seq=21L, ack=12L, flags="A", sport=1234, dport=80),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.1', dst='10.0.0.2') / TCP(seq=21L, ack=11L, flags="PA", sport=1234, dport=80) / Raw(load='REPLY ' * 3),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.2', dst='10.0.0.1') / TCP(seq=11L, ack=22L, flags="A", sport=80, dport=1234),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.2', dst='10.0.0.1') / TCP(seq=11L, flags="F", sport=80, dport=1234),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.1', dst='10.0.0.2') / TCP(seq=11L, ack=12L, flags="A", sport=1234, dport=80),
        Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.1', dst='10.0.0.2') / TCP(seq=11L, flags="F", sport=1234, dport=80),
        Ether(src=smac, dst=dmac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.2', dst='10.0.0.1') / TCP(seq=11L, ack=12L, flags="A", sport=80, dport=1234),
    ]

if args.handshake == 2:
    session[1][TCP].flags = "A"
    if args.ipv6:
        session.insert(2, Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::1', dst='::2') / TCP(seq=20L, ack=11L, flags="S", sport=1234, dport=80))
    else:
        session.insert(2, Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.1', dst='10.0.0.2') / TCP(seq=20L, ack=11L, flags="S", sport=1234, dport=80))
elif args.handshake == 3:
    session[1][TCP].flags = "S"
    if args.ipv6:
        session.insert(2, Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IPv6(src='::1', dst='::2') / TCP(seq=20L, ack=11L, flags="A", sport=1234, dport=80))
    else:
        session.insert(2, Ether(src=dmac, dst=smac) / Dot1Q(vlan=10, prio=5) / IP(src='10.0.0.1', dst='10.0.0.2') / TCP(seq=20L, ack=11L, flags="A", sport=1234, dport=80))


def conversation(src, dst, sport, dport):
    global session
    #sport = (sport + session) % 60000 + 5535
    s = long(sport) + randint(63534L, 63533L*63534L) # to make sequence numbers unique
    for p in session:
        ip = p[IPv6] if args.ipv6 else p[IP]
        tcp = p[TCP]
        qh = p[Dot1Q]
        p.vlan = (sport + dport) % 3 + 1
        p.prio = ((sport + dport) % 2) * 10
        if args.ipv6:
            ip.tc = ((sport + dport) % 254) + 1
        else:
            ip.tos = ((sport + dport) % 254) + 1

        if p[Ether].src == smac: # client
            ip.src, ip.dst = src, dst
            tcp.sport, tcp.dport = sport, dport
        else:
            ip.src, ip.dst = dst, src
            tcp.sport, tcp.dport = dport, sport
        yield p
        tcp.seq += 13L
        if tcp.ack != 0:
            tcp.ack += 13


if args.port is None:
    erffile = open(args.output, 'wb')
else:
    s = vision.create_socket(host=args.host, port=args.port)
    erffile = s.makefile('w')

ts = ts0
for (d, d_change), (hr, hr_change), (m, m_change) in product(enumerate(monthly_trend), enumerate(daily_trend), enumerate(hourly_trend)):
    if args.terminate is None and hr >= 1:  # default termination
        break
    ts1 = ts0 + 60 * (m + 60 * (hr + 24 * d))
    if ts1 < ts:  # all trend lists exceeded
        break
    for i, (rel_ts, min_l, max_l, avg_l, retries) in enumerate(profile):
        ts = ts1 + rel_ts
        # adjust to the trend change:
        if args.validation:
            avg_l = ((m * 60) + int(rel_ts)) % 42 + 1
            min_l = min(1, avg_l - int(rel_ts) % 7)
            min_l = max(100, avg_l + int(rel_ts) % 7 + 1)
        else:
            min_l, max_l, avg_l = map( lambda l: (1. + m_change) * (1. + hr_change) * (1. + d_change) * l, (min_l, max_l, avg_l))
        if args.summary:
            print >>summary_csv, "{0}, {1}, {2}, {3}, {4}".format(ts, min_l, avg_l, max_l, retries)
        # RTT to get the required average:
        rtt = ((avg_l * len(src_dst) - ( min_l + max_l)) / (len(src_dst) - 2.)) / 1000
        # "distance" between packet so that all conversations can be done in 1sec:
        delay = ( 1 - len(src_dst) * rtt - (min_l + max_l)/1000. ) / len(src_dst) / 9
        if args.retransmission:
            delay -= retries/1000.  # assuming that there are only 1ms between retransmissons
        for conv_no, (src, dst, sport, dport) in enumerate(src_dst):
            conv_start = ts

            for p_no, p in enumerate(conversation(src, dst, (sport+i+m) % 63534 + 2000, dport)):
                if args.terminate < ts - ts0:
                    break
                #erffile.write(vision.to_erf(p, ts)) if args.port is None else vision.write_erf(erffile, p, ts)
                try:
                    vision.write_erf(erffile, p, ts)
                except Exception as e:
                    print '=' * 80
                    print e
                    print '=' * 80
                    print p
                    print '=' * 80
                if args.summary and ((handshake == 1 and p_no == 2) or (handshake > 1 and p_no == 3)):
                    print >>irtt_csv, "{0}, {1}, {2}, {3}".format(ts, src, dst, (ts - conv_start))

                if handshake == 1 and (p_no == 0 or p_no == 1): # next packet will be handshake ACK/SYN or ACK
                    if conv_no > 1:
                        ts += rtt / 2.
                    elif conv_no == 0:
                        # min IRTT
                        ts += min_l / 2000.
                    elif conv_no == 1: # max IRTT
                        ts += max_l / 2000.
                elif handshake > 1 and (p_no <= 2): # next packet will be handshake ACK, SYN or ACK
                    if conv_no > 1:
                        ts += rtt / 3.
                    elif conv_no == 0:
                        # min IRTT
                        ts += min_l / 3000.
                    elif conv_no == 1: # max IRTT
                        ts += max_l / 3000.
                else:
                    ts += delay
            else:
                if args.retransmission and retries > 0:  # retransmit the last packet of the last conversation
                    #ts -= (delay - 0.001)
                    for _ in xrange(retries):
                        vision.write_erf(erffile, p, ts)
                        ts += 0.001


    if args.terminate < (ts - ts0):
        break

if args.summary:
    summary_csv.close()
    irtt_csv.close()

if args.port is None:
    erffile.close()
else:
    s.close()
