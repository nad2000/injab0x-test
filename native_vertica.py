#!/usr/bin/python
"""
 1 start_time_sec     | integer  | 4
 2 mac_addr_1         | binary(6)| 6
 3 mac_addr_2         | binary(6)| 6
 4 byte_count_1       | bigint   | 8
 5 byte_count_2       | bigint   | 8
 6 packet_count_1     | bigint   | 8
 7 packet_count_2     | bigint   | 8
 8 last_time_sec      | integer  | 4
 9 vlan_id            | smallint | 2 NULL
10 vlan_id2           | smallint | 2 NULL
11 mpls_tag           | integer  | 4 NULL
12 mpls_tag2          | integer  | 4 NULL
13 application_id     | integer  | 4 NULL
14 ip_addr_1          | varbinary(16) -1
15 ip_addr_2          | varbinary(16) -1
16 port_1             | uint2    | 4 NULL
17 port_2             | uint2    | 4 NULL
18 ip_protocol        | uint1    | 2
19 zero_window_events | smallint | 2 NULL
20 fragmented_count   | integer  | 4 NULL
21 diff_serv          | uint1    | 2
22 flags              | uint1    | 2
23 flow_id            | bigint   | 8


create table samples (
    start_time_sec int, 
    mac_addr_1 binary(6),
    mac_addr_2 binary(6),
    byte_count_1 int,
    byte_count_2 int,
    packet_count_1 int,
    packet_count_2 int,
    last_time_sec int,
    vlan_id int, 
    vlan_id2 int, 
    mpls_tag int,
    mpls_tag2 int,
    application_id int,
    ip_addr_1 varbinary(16),
    ip_addr_2 varbinary(16),
    port_1 int,
    port_2 int,
    ip_protocol int,
    zero_window_events int, 
    fragmented_count int,
    diff_serv int,
    flags int,
    flow_id int);



"""
from struct import *
import socket
import sys


def str2int(str):
    if str == '':
        return 0
    else:
        return int(str)


def mac2bin(str):
    return pack('=q', int(str.replace(':', ''), 16))[:6]


def ipaddr2varbin(str):
    if ':' in str:
        return '\x10\0\0\0' + socket.inet_pton(socket.AF_INET6, str)
    else:
        return '\x04\0\0\0' + socket.inet_aton(str)

data = """12,ff:1a:c5:00:04:a6,01:00:5e:7f:00:01,814,0,11,0,22,-1,-1,-1,-1,522,192.168.132.98,239.255.0.1,54506,5577,17,0,0,0,0,1
15,ff:2f:ff:43:ae:00,00:30:48:9f:7a:96,9697,118997,41,39,25,-1,-1,-1,-1,216,192.168.129.43,192.168.132.111,55583,80,6,0,0,0,0,2
21,ff:3f:4f:9f:7a:b0,01:00:5e:02:0b:47,13280,0,130,0,31,-1,-1,-1,-1,522,192.168.132.113,239.2.11.71,45597,8649,17,0,0,0,0,4
12,ff:1f:cf:00:04:a6,01:00:5e:7f:00:01,1554,0,21,0,32,-1,-1,-1,-1,522,192.168.132.98,239.255.0.1,54506,5577,17,0,0,0,0,1
15,ff:2f:ff:43:ae:00,00:30:48:9f:7a:96,13375,132199,55,61,36,-1,-1,-1,-1,216,192.168.129.43,192.168.132.111,55583,80,6,0,0,0,0,2
12,ff:1f:cf:00:04:a6,01:00:5e:7f:00:01,2294,0,31,0,42,-1,-1,-1,-1,522,192.168.132.98,239.255.0.1,54506,5577,17,0,0,0,0,1
21,ff:3f:4f:9f:7a:b0,01:00:5e:02:0b:47,26618,0,261,0,47,-1,-1,-1,-1,522,192.168.132.113,239.2.11.71,45597,8649,17,0,0,0,0,4
15,ff:2f:ff:43:ae:00,00:30:48:9f:7a:96,16021,167857,66,77,50,-1,-1,-1,-1,216,192.168.129.43,192.168.132.111,55583,80,6,0,0,0,0,2
12,ff:1f:cf:00:04:a6,01:00:5e:7f:00:01,3034,0,41,0,52,-1,-1,-1,-1,522,192.168.132.98,239.255.0.1,54506,5577,17,0,0,0,0,1
26,ff:3f:4f:9f:7a:96,00:26:f1:43:ae:00,140,140,2,2,30,-1,-1,-1,-1,216,192.168.132.111,192.168.129.43,80,55579,6,0,0,0,1,5
27,ff:3f:4f:9f:7a:96,00:26:f1:43:ae:00,140,140,2,2,30,,,,,,192.168.132.111,192.168.129.43,,,6,,,0,1,6
"""

cols = (4, 6, 6, 8, 8, 8, 8, 4, 2, 2, 4, 4, 4, -1, -1, 4, 4, 2, 2, 4, 2, 2, 8)
ccount = len(cols)

if len(sys.argv) > 1 and sys.argv[1] != '':
    ofn = sys.argv[1]
else:
    ofn = '/data/native_vertica.bin'

f = open(ofn, 'wb')
f.write('NATIVE\n\xff\r\n\x00')
f.write(pack('=ihbh', ccount * 4 + 5, 1, 0, ccount))
f.write(pack('=' + 'i' * ccount, *cols))

for l in sys.stdin:
    # for l in data.splitlines():
    row = l.split(',')
    orow = pack('=i', int(row[0])) + mac2bin(row[1]) + mac2bin(row[2]) +\
        pack('=qqqqihhiii', *map(str2int, row[3:13])) +\
        ipaddr2varbin(row[13]) + ipaddr2varbin(row[14]) +\
        pack('=iihhihhq', *map(str2int, row[15:]))
    f.write(pack('=i', len(orow)))
    f.write('\x00\x00\x00')
    f.write(orow)

f.close()

##    row = l.split(',')
##    nulls = pack('>I', reduce( lambda x,y: (x<<1)+(1 if y=='' else 0),row,0) << 9 )[:3]
# orow = pack('=i',int(row[0]))+mac2bin(row[1])+mac2bin(row[2])+\
# pack('=lllli',*map(int,row[3:8]))
# if row[8]!='':
##        orow += pack('=h',int(row[8]))
# if row[9]!='':
##        orow += pack('=h',int(row[9]))
# orow += ''.join(map( lambda x: pack('=i',int(x)) if x!='' else '', row[10:13]))+\
# ipaddr2varbin(row[13])+ipaddr2varbin(row[14])
# if row[15]!='':
##        orow += pack('=i',int(row[15]))
# if row[16]!='':
##        orow += pack('=i',int(row[16]))
##    orow += pack('=h',int(row[17]))
# if row[18]!='':
##        orow += pack('=h',int(row[18]))
# if row[19]!='':
##        orow += pack('=i',int(row[19]))
##    orow += pack('=hhl',*map(int,row[20:]))
# f.write(pack('=i',len(orow)))
# f.write(nulls)
# f.write(orow)
