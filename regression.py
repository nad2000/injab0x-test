#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Legacy Vision testing:

 - virtualevn ~/testenv
 - . ~/testenv/bin/activate
 - pip install -U -r requirements.txt
 - py.test -s regression.py

"""

from fixtures import *
from selenium.webdriver.support.ui import Select

#
# Prameters:
# startign timestamp or 0 (the test will start with the smallest available value for the view)
# eg, datetime.datetime.strptime('Wed, 7 Aug 2013 06:54:00', '%a, %d %b %Y %H:%M:%S').strftime("%s")
ts_start = 0  # or 0
ts_step = 60

default_rotfile = 'latency1h'
default_vis_name = default_rotfile + "_vis"
default_tracefile = default_rotfile + '.erf'

#%%

def create_app_vis(
        probe=default_probe,
        name=None,
        rotfile=default_rotfile,
        tracefile=default_tracefile,
        replay=True,
        clean=False,
        vis=None):
    """
    Create application breakdown with 4 top applications filtered-out

    name - name of the vision view
    rotfile - view over this rotfile (captured traffic)
    replay - capture with "replay" (for uView testing, you can replay=False)
    """

    cli = vision.Cli(probe=probe, verbose=True)
    cli.capture_efrfile_from_netsource(rotfile=rotfile, tracefile=tracefile, clean=clean)

    if name is None:
        name = rotfile + "_vis"

    if vis is None:
        vis = vision.Vision(probe=probe)
    else:
        vis.get_vision()

    vis.delete_visualization(name)
    #vis.delete_all_visualization()

    vis.get_vision()
    driver = vis.driver

    driver.find_element_by_id("add_vis_link").click()
    driver.find_element_by_id("add_vis_name").clear()
    driver.find_element_by_id("add_vis_name").send_keys(name)

    Select(driver.find_element_by_name("add_vis_ids")
           ).select_by_visible_text(rotfile)
    driver.find_element_by_id("vis_type_2").click()
    driver.find_element_by_id("form_submit").click()
    vis.wait_until_complete()

    view = vis.view_id

    # Change to 'Application':
    vis.breakdown_type = "Application"

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    return vis


@pytest.fixture(scope="session")
def app_vis(request):
    """
    Create application breakdown with 4 top applications filtered-out
    """

    vis = create_app_vis(name=default_vis_name)
    vis.breakdown_type = "Application"
    vis.check_select("tcp")
    vis.filter_in_selected()

    vis.breakdown_type = "Client IP Address"
    vis.check_select("172.16.1.1")
    vis.filter_out_selected()

    vis.breakdown_type = "Destination IP Address"
    vis.check_select("192.168.1.124")
    vis.filter_out_selected()

    vis.breakdown_type = "Source Port"
    vis.check_select("TCP/13088")
    vis.filter_out_selected()

    vis.breakdown_type = "Destination Port"
    vis.check_select("TCP/37691")
    vis.filter_out_selected()

    vis.breakdown_type = "Client Port"
    vis.check_select("TCP/47403")
    vis.check_select("TCP/34492")
    vis.check_select("TCP/37045")
    vis.filter_out_selected()

    vis.breakdown_type = "Destination MAC"
    vis.check_select("10:60:4b:5a:c6:15")
    vis.check_select("00:26:f1:43:ae:00")
    vis.filter_in_selected()

    vis.breakdown_type = "Source MAC"
    vis.check_select("10:60:4b:5a:c6:15")
    vis.check_select("00:26:f1:43:ae:00")
    vis.filter_in_selected()

    vis.breakdown_type = "IP Protocol"
    vis.check_select("TCP")
    vis.filter_in_selected()

    vis.breakdown_type = "IP Version"
    vis.check_select("IPv4")
    vis.filter_in_selected()

    def fin():
        print "### Finalizing ###"
        try:
            #vis.delete_visualization(name=default_vis_name)
            vis.close_driver()
        except:
            pass
    request.addfinalizer(fin)

    return vis


@pytest.fixture(scope="session")
def uview_ot_vis():
    """
    Create application breakdown with 4 top applications filtered-out
    for uView tests
    """

    # we need to restart DBMGR because otherwise the uviews from previous run are reused
    cli = vision.Cli(probe=default_probe)
    cli.restart_service("dbmgr")
    time.sleep(5)
    cli.save_config()
    cli.close()

    name = "uview_" + default_vis_name
    vis = create_app_vis(name=name)
    driver = vis.driver

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN_OVER_TIME

    def fin():
        print "### Finalizing ###"
        try:
            vis.close_driver()
        except:
            pass

    return vis


@pytest.mark.parametrize("breakdown_type", [
    "Application",
    "Data Source",
    "IP Protocol",
    "IP Version",
    "VLAN",
    "MPLS"])
def test_single_value_breakdown_vis(app_vis, breakdown_type):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    vis.breakdown_type = breakdown_type
    vis.view_type = vision.TRAFFIC_BREAKDOWN
    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    lines = vis.overview_lines
    assert len(lines) == 1, "There should only be IPv4. Only one entry in summary."
    assert lines.values()[0] == "219 Mb"


@pytest.mark.parametrize("breakdown_type", [
    "Application", "Data Source", "IP Protocol", "IP Version", "VLAN", "MPLS",
    "Source Port", "Destination Port", "Server Port", "Client Port",
    "Source IP Address", "Destination IP Address", "Server IP Address", "Client IP Address",
    "Source MAC", "Destination MAC"])
def test_TOT(app_vis, breakdown_type):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id
    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    vis.breakdown_type =  breakdown_type
    vis.view_type = vision.TRAFFIC_BREAKDOWN_OVER_TIME

    assert driver.find_elements_by_css_selector("g > g > path"), "Hightchart char should be drawn"
    #series_count = len(driver.find_elements_by_xpath("//svg/g/g[@class='highcharts-series']"))
    series_count = len(driver.find_elements_by_css_selector(
        "div.highcharts-container > svg > g.highcharts-series-group > g.highcharts-series")) - 2
    lines = vis.overview_lines
    assert len(lines) == series_count
    if breakdown_type in ["Application", "Data Source", "IP Protocol", "IP Version", "VLAN", "MPLS",]:
        assert len(lines) == 1

    if breakdown_type == "Data Source":
        assert default_rotfile in lines.keys()[0]
    elif breakdown_type == "Application":
        assert lines.keys()[0] == "tcp"
    elif breakdown_type == "IP Protocol":
        assert lines.keys()[0] == "TCP"
    elif breakdown_type == "IP Version":
        assert lines.keys()[0] == "IPv4"
    elif breakdown_type == "VLAN":
        assert lines.keys()[0] == "No VLAN"
    elif breakdown_type == "MPLS":
        assert lines.keys()[0] == "No MPLS"
    elif breakdown_type== "Source Port":
        #assert len(lines) == 9
        assert set(lines) > set(["TCP/80: http", "TCP/443: https", "TCP/25: smtp", "TCP/8080: http-alt", "Other"])
    elif breakdown_type== "Destination Port":
        #assert len(lines) == 11
        assert set(lines) > set(['Other', 'TCP/16596', 'TCP/25: smtp', 'TCP/28078', 'TCP/443: https', 'TCP/47292'])
    elif breakdown_type== "Server Port":
        #assert len(lines) == 4
        assert set(lines) == set(["TCP/80: http", "TCP/443: https", "TCP/25: smtp", "TCP/8080: http-alt"])
    elif breakdown_type== "Client Port":
        #assert len(lines) == 11
        assert set(lines) > set(['Other', 'TCP/14406', 'TCP/14407', 'TCP/16596', 'TCP/34870', 'TCP/47291'])
    elif breakdown_type== "Source IP Address":
        #assert len(lines) == 11
        assert set(lines) == set(["192.168.1.140", "192.168.1.118", "192.168.1.125",
                             "192.168.1.147", "192.168.1.103", "192.168.1.130",
                             "192.168.1.150", "192.168.1.148", "192.168.1.134",
                             "192.168.1.113", "Other"])
    elif breakdown_type== "Destination IP Address":
        #assert len(lines) == 11
        assert set(lines) < set(["192.168.1.104", "192.168.1.127", "192.168.1.129",
                             "192.168.1.122", "192.168.1.119", "192.168.1.112",
                             "192.168.1.151", "192.168.1.121", "192.168.1.132",
                             "192.168.1.109", '192.168.1.110', "Other"])
    elif breakdown_type== "Server IP Address":
        #assert len(lines) == 11
        assert set(lines) < set(["192.168.1.127", "192.168.1.122", "192.168.1.129",
                             "192.168.1.151", "192.168.1.132", "192.168.1.121",
                             "192.168.1.110", "192.168.1.104", "192.168.1.109",
                             "192.168.1.119", "Other"])
    elif breakdown_type== "Client IP Address":
        #assert len(lines) == 11
        assert set(lines) < set(["192.168.1.125", "192.168.1.103", "192.168.1.147",
                                  "192.168.1.150", "192.168.1.130", "192.168.1.140",
                                  "192.168.1.148", '192.168.1.113', '192.168.1.134',
                                  '192.168.1.118', "Other"])
    elif breakdown_type in ["Source MAC", "Destination MAC"]:
        assert len(lines) == 2
        assert set(lines) == set(["00:26:f1:43:ae:00", "10:60:4b:5a:c6:15"])


@pytest.mark.parametrize("breakdown_type", ["Source MAC", "Destination MAC"])
def test_mac_breakdown_vis(app_vis, breakdown_type):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    vis.breakdown_type = breakdown_type
    vis.view_type = vision.TRAFFIC_BREAKDOWN

    lines = vis.overview_lines
    if "Source" in breakdown_type:
        assert lines['00:26:f1:43:ae:00'] == "116 Mb"
        assert lines['10:60:4b:5a:c6:15'] == "104 Mb"
    else:
        assert lines['00:26:f1:43:ae:00'] == "104 Mb"
        assert lines['10:60:4b:5a:c6:15'] == "116 Mb"
    assert len(lines) == 2, "There should only two lines in the summary."


@pytest.mark.parametrize("breakdown_type", ["Source Port", "Destination Port", "Server Port", "Client Port"])
def test_port_breakdown_vis(app_vis, breakdown_type):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()
    vis.breakdown_type = breakdown_type

    lines = vis.overview_lines
    if breakdown_type == "Source Port":
        assert lines["TCP/80: http"] == "56 Mb"
        assert lines["TCP/443: https"] == "24 Mb"
        assert lines["TCP/25: smtp"] == "16 Mb"
        assert lines["TCP/8080: http-alt"] == "8 Mb"
        assert lines["Other"] == "115 Mb"
    elif breakdown_type == "Destination Port":
        assert lines["TCP/80: http"] == "58 Mb"
        assert lines["TCP/443: https"] == "29 Mb"
        assert lines["TCP/25: smtp"] == "19 Mb"
        assert lines["TCP/8080: http-alt"] == "10 Mb"
        assert lines["TCP/16596"] == "136 Kb"
        assert lines["Other"] == "103 Mb"
    elif breakdown_type == "Server Port":
        assert lines["TCP/80: http"] == "114 Mb"
        assert lines["TCP/443: https"] == "53 Mb"
        assert lines["TCP/25: smtp"] == "35 Mb"
        assert lines["TCP/8080: http-alt"] == "18 Mb"
    else:
        assert len(lines) == 11
        for k in lines:
            if k == "Other":
                assert lines["Other"] == "216 Mb"
            elif k in ["TCP/47293", "TCP/14407", "TCP/47291"]:
                assert lines[k] == "295 Kb"
            elif k in ["TCP/47292", "TCP/64244", "TCP/56414"]:
                assert lines[k] == "300 Kb"


def test_result_limit(app_vis):

    vis = app_vis
    driver = vis.driver
    name = default_vis_name

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    vis.breakdown_type = "Client Port"
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("5")

    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN

    assert len(vis.overview_lines) == 6, "Overview should have 6 entries in total"  # 5 + 1 (Other)


@pytest.mark.parametrize("breakdown_type", [
    "Application",
    "Data Source",
    "IP Protocol",
    "IP Version",
    "VLAN",
    "MPLS",
    "Source Port",
    "Destination Port",
    "Source IP Address",
    "Destination IP Address",
    "Source MAC",
    "Destination MAC"])
def test_uview_breakdown_tot(uview_ot_vis, breakdown_type):

    vis = uview_ot_vis
    driver = vis.driver
    vis.breakdown_type = breakdown_type

    view = vis.view_id
    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()
    start, _ = vis.data_time_range
    vis.query_time_range = (start + 600, start + 601.1)

    assert driver.find_elements_by_css_selector("g > g > path"), "Hightchart char should be drawn"
    # TODO: some more condition tests


@pytest.mark.parametrize("breakdown_type", ["Source IP Address", "Destination IP Address"])
def test_VP_172(breakdown_type):

    name = "VP-172: " + breakdown_type

    vis = create_app_vis(name=name)

    vis.breakdown_type = breakdown_type
    vis.check_select("192.168.1.104" if "Destination" in breakdown_type else "192.168.1.103")
    vis.filter_out_selected()

    driver = vis.driver
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    vis.breakdown_type = breakdown_type
    vis.view_type = vision.TRAFFIC_BREAKDOWN

    lines = vis.overview_lines
    assert len(lines) > 0
    if "Source" in breakdown_type:
        assert lines["192.168.1.140"] == "10 Mb"
    else:
        assert lines["192.168.1.124"] == "10 Mb"
    assert len(lines) == 11


@pytest.mark.parametrize("breakdown_type", ["Source Port", "Destination Port"])
def test_EP_2347(breakdown_type):

    name = "EP-2347: " + breakdown_type

    vis = create_app_vis(name=name)

    vis.breakdown_type = breakdown_type
    vis.check_select("TCP/80: http")
    vis.filter_out_selected()

    driver = vis.driver
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    vis.breakdown_type = breakdown_type
    vis.view_type = vision.TRAFFIC_BREAKDOWN

    lines = vis.overview_lines
    assert len(lines) > 0
    if "Source" in breakdown_type:
        assert lines["TCP/443: https"] == "32 Mb"
        assert lines["TCP/25: smtp"] == "16 Mb"
        assert lines["TCP/8080: http-alt"] == "8 Mb"
    else:
        assert lines["TCP/443: https"] == "39 Mb"
        assert lines["TCP/25: smtp"] == "19 Mb"
        assert lines["TCP/8080: http-alt"] == "10 Mb"
    assert len(lines) == 11
