#!/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
"""
Created on Fri Apr 10 13:44:27 2015

@author: rad.cirskis
"""

import json
from urllib2 import urlopen, Request
import os
import time
import hashlib
import hmac
import vision
import pytest
from collections import namedtuple

default_rotfile = 'latency_IPv6_1h'
default_vis_name = default_rotfile + "_vis"
default_tracefile = default_rotfile + '.erf'
default_probe = os.environ.get("TARGET_HOST", "192.168.132.111")

#%%

class Client(object):

    probe_ = None
    last_res_ = None

    # common query parameters
    start_time = "2015-02-13T05:35:42Z"
    end_time = "2015-02-13T05:45:42Z"


    def __init__(self, probe=None, rotfile=None, verbose=False):
        self.verbose = verbose
        self.probe =  default_probe if probe is None else probe
        self.rotfile = default_rotfile if rotfile is None else rotfile

    @property
    def probe(self):
        return self.probe_

    @property
    def response(self):
        return self.last_res_

    @property
    def request_id(self):
        """Last reqest ID"""
        return self.response.get("clientID")

    @probe.setter
    def probe(self, value):
        self.probe_ = os.environ.get("TARGET_HOST", default_probe) if value is None else value
        self.url = "http://" + self.probe_ + ":9097/vision-query-service/api-queries/v2/queries"

    @property
    def auth_header_val(self):
        ts = str(int(time.time()))
        val = "testUser" + ts
        h = hmac.new("erp_auth_KEY+", val, hashlib.sha1)
        return "ENDACE testUser:{0}_{1}".format(h.hexdigest(), ts)

    def post(self, q):

        if not (type(q) is str):
            q = json.dumps(q)

        headers = {
            'Content-Type': 'application/json',
            'Authorization': self.auth_header_val,
        }
        req = Request(self.url, q, headers)
        response = urlopen(req)
        raw_data = response.read().decode('utf-8')
        response.close()

        self.last_res_ = json.loads(raw_data)
        return self.last_res_


    def get(self, id=None, path=None, number_of_results=100):
        """
        Retrieves query results or a direct path
        """

        if id is None and path is None:
            id = self.request_id

        if id is None and path is None:
            return None

        headers = {
            'Content-Type': 'application/json',
            'Authorization': self.auth_header_val,
        }
        if id is None:
            if path.startswith("http"):
                url = path
            else:
                url = ("http://" + self.probe_ + ":9097" +
                    + (path if path.startswith('/') else "/" + path))
        else:
            url = self.url + "/" + id + "?number_of_results=" + str(number_of_results)
        #print url
        req = Request(url, headers=headers)

        for _ in xrange(120):  # 120 attempts 1 sec apart
            response = urlopen(req)
            raw_data = response.read().decode('utf-8')
            response.close()
            res = json.loads(raw_data)
            if id is None or res["meta"]["complete"]:
                break
            time.sleep(1)
        else:
            return None

        self.last_res_ = res
        return self.last_res_

    @property
    def datasources(self):
        res = self.get(path="http://" + self.probe + ":9011/api-datasource/v1/datasources")["payload"]
        sources = [namedtuple('DataSource', s.keys())(**s) for s in res]
        return sources

    def datasource(self, probe=None, name=None):
        if probe is None:
            probe = self.probe
        if name is None:
            name = self.rotfile

        datasources = self.datasources

        for s in datasources:
            if s.probeName == probe and s.name == name:
                return s
        else:
            for s in datasources:
                if s.name == name:
                    return s
            else:
                return None

    def query(self, q):
        self.post(q)
        id = self.request_id
        return self.get(id=id)

    def traffic_breakdown(self, breakdown_type="datasource", start=None, end=None, data_sources=[],
                          query_filter={}):
        q = {
            "breakdown_type": breakdown_type,
            "datasource_guids": [self.datasource().id] if data_sources == [] else data_sources,
            "auto_pivot": True,
            "other_required": True,
            "start_time": (self.start_time if start is None else start),
            "end_time": (self.end_time if end is None else end),
            "order_by": "bytes",
            "order_direction": "desc",
            "points": 1,
            "type": "TrafficBreakdown"
        }
        if query_filter != {}:
            q["filter"] = query_filter
        return self.query(q)

    def conversation(self, breakdown_type="ninetuple", start=None, end=None, data_sources=[],
                          query_filter={}):
        q = {
            "type": "ConversationsNG",
            "breakdown_type": breakdown_type,
            "datasource_guids": [self.datasource().id] if data_sources == [] else data_sources,
            "auto_pivot": True,
            "other_required": True,
            "disable_uvision": False,
            "start_time": (self.start_time if start is None else start),
            "end_time": (self.end_time if end is None else end),
            "order_by": "bitrate_total",
            "order_direction": "desc",
            "inactivity_timeout": 15,
            "points": 1,
        }
        if query_filter != {}:
            q["filter"] = query_filter
        return self.query(q)


@pytest.fixture(scope="session")
def capture_traffic(
    probe=default_probe,
    rotfile=default_rotfile,
    replay=False,
    tracefile=default_tracefile,
    clean=False):

    cli = vision.Cli(probe=probe, user="root", verbose=True)
    cli.capture_efrfile_from_netsource(rotfile=rotfile, replay=replay, tracefile=tracefile, clean=clean)

    def fin():
        print "### Finalizing ###"
        try:
            cli.delete_pipe(rotfile+'_pipe')
            cli.delete_rotfile(rotfile)
            cli.close()
        except:
            pass

    return cli


@pytest.mark.parametrize("breakdown_type, expected", [
    ("app", 5253360),
    ("sip", 205320),
    ("dip", 205320),
    ("dmac", 2874480),
    ("smac", 2874480),
    ("dport", 1437240),
    ("sport", 1189440),
    ("ipversion", 5253360),
    ("mpls", 5253360),
    ("prot", 5253360),
    ("vlan", 5253360),
    ("serverip", 375240),
    ("clientip", 375240),
    ("datasource", 5253360),])
def test_vqs_breakdown(capture_traffic, breakdown_type, expected):

    cli = capture_traffic

    vqs = Client(probe=cli.probe, rotfile=default_rotfile)
    data_source = vqs.datasource()


    start_time = "2015-02-13T05:35:42Z" # data_source.startMetadataTime  #
    end_time = "2015-02-13T05:45:42Z"   # data_source.endMetadataTime    #

    res = vqs.traffic_breakdown(breakdown_type, start_time, end_time, data_sources=[data_source.id])
    #print res

    assert res["payload"]["totals"][0]["aggregates"]["bytes"] == expected


@pytest.mark.parametrize("breakdown_type, expected", [
    ("app", 4878120),
    ("sip", 205320),
    ("dip", 205320),
    ("dmac", 2669160),
    ("smac", 2669160),
    ("dport", 1437240),
    ("sport", 1189440),
    ("ipversion", 4878120),
    ("mpls", 4878120),
    ("prot", 4878120),
    ("vlan", 4878120),
    ("serverip", 375240),
    ("clientip", 375240),
    ("datasource", 4878120),])
def test_vqs_breakdown_with_filter(capture_traffic, breakdown_type, expected):

    cli = capture_traffic

    vqs = Client(probe=cli.probe, rotfile=default_rotfile)
    data_source = vqs.datasource()


    start_time = "2015-02-13T05:35:42Z" # data_source.startMetadataTime  #
    end_time = "2015-02-13T05:45:42Z"   # data_source.endMetadataTime    #

    res = vqs.traffic_breakdown(breakdown_type, start_time, end_time, data_sources=[data_source.id],
                                query_filter=dict(
                                    type="includeexclude", data=dict(
                                        exclude=dict(ip="172.16.1.1"),
                                        include=dict(ipversion="6", prot="6")
                                    )
                                ))
    #print res
    assert res["payload"]["totals"][0]["aggregates"]["bytes"] == expected


def test_VNG831(capture_traffic):
    """
    VNG-831: Latency column is empty in Conversations session view
    https://jira.emulex.com/browse/VNG-831
    """

    cli = capture_traffic

    vqs = Client(probe=cli.probe, rotfile=default_rotfile)
    data_source = vqs.datasource()


    start_time = "2015-02-13T05:35:42Z" # data_source.startMetadataTime  #
    end_time = "2015-02-13T05:36:42Z"   # data_source.endMetadataTime    #

    res = vqs.conversation( start=start_time, end=end_time, data_sources=[data_source.id],
                                query_filter=dict(
                                    type="includeexclude", data=dict(
                                        exclude=dict(ip="172.16.1.1"),
                                        include=dict(ipversion="6", prot="6")
                                    )
                                ))
    #print res
    #all latency values should be definite and >0
    assert all([l is not None and l > 0 for l in [s["aggregates"]["latency"] for s in res["payload"]["intervals"][0]["series"]]])
