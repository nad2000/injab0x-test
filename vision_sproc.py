# -*- coding: utf-8 -*-

#!/usr/bin/env python

import unittest
import time
import vision
import os
import pg8000
import testdb

#
# Prameters:
default_probe = os.getenv('TARGET_HOST')
#

#%%

#db = pg8000.DBAPI.connect(host=probe, user="postgres", database="postgres")
#cr = db.cursor()
#cr.execute("select * from erfstores")
# print cr.fetchall()


#%%
class Sprocs(vision.TestCase):
    probe = dafult_probe
    assert probe is not None
    @classmethod
    def setUpClass(cls):
        """Creates test erfstore DB"""

        try:
            # testdb.create_testdb(probe)
            pass
        except:
            pass

    @classmethod
    def tearDownClass(cls):
        """Drops test erfstore DB"""
        try:
            db = pg8000.connect(
                host=probe, user="postgres", database="postgres")
            #db.execute("DROP DATABASE IF EXISTS testdb")
            db.close()
        except:
            pass

    def tearDown(self):
        try:
            self.__db.close()
        except:
            pass

    @property
    def db(self):
        """Connection to testdb"""
        try:
            db = self.__db
        except:
            db = pg8000.connect(
                host=probe, user="postgres", database="testdb")
            self.__db = db
        return db

    def test_sproc(self):
        """Basic breakdown_summary_sql test"""

        q = list(
            self.db.execute(
                "SELECT breakdown_summary_sql('sport',000,111,'zwin')"
            ))[0][0]

        self.assertTrue(
            "zero_window_events" in q,
            msg='If break down traffic unit is zerrow window event, zero_window_events should be present in the query')

    @unittest.skip
    def test_alerts(self):
        self.db.execute("""
            DELETE FROM alerts
            WHERE timestamp_sec BETWEEN 1368588502 AND 1368588541
              AND alarm_id=1 AND alarm_name='bw200Mbps';

            INSERT INTO alerts VALUES
              (1368588502, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', true),
              (1368588506, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', false),
              (1368588507, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', true),
              (1368588509, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', false),
              (1368588510, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', false),
              (1368588511, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', true),
              (1368588514, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', true),
              (1368588517, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', false),
              (1368588518, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', true),
              (1368588526, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', false),
              (1368588536, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', true),
              (1368588538, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', false),
              (1368588539, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', true),
              (1368588541, '0', 200000000, '2', -1, '1', '5', '1', 'bw200Mbps', '', false);
              """)

        for r in self.db.execute("""
                SELECT duration::int, start_timestamp_sec, end_timestamp_sec
                FROM alarm_history(1,NULL,NULL,'time_desc',10,NULL)"""):
            self.assertTrue(
                r[0] > 0,
                'Duration should be non-negative: %s (start TS: %s, end TS: %s)' %
                tuple(r))


if __name__ == "__main__":
    unittest.main()
