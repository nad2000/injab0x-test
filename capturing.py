#!/usr/bin/env python

import time
from vision import Vision, create_socket, write_erf
from scapy.all import Ether, IP, IPv6, ARP, UDP, Raw, Padding
import os
import pytest

#
# Prameters:
default_probe = os.environ.get("TARGET_HOST")
default_netsource_port = 7777
@pytest.fixture(scope="module")
def vision(probe=default_probe):
    return Vision(probe=probe)

def nc_packets(packets, host=default_probe, port=default_netsource_port, times=1):
    """'netcat's multiple timest packets wrapped inot ERF records to
    specified prot of the probe under the test."""
    s = create_socket(host, port)
    f = s.makefile('w')
    for _ in xrange(times):
        for p in packets:
            write_erf(f,p)
    s.close()


def test_capturing():

    rotfile = 'zero'  # 'small'
    pipe_name = rotfile+'_pipe'
    source_port = default_netsource_port

    vis = vision(probe=default_probe)

    vis.headless = os.environ.get('HEADLESS', False)
    vis.auto_screenshot = True

    driver = vis.driver
    driver.get(vis.base_url+vis.resources_url)
    vis.remove_rotfile(rotfile)
    vis.create_rotfile(name=rotfile)
    source_name = 'p' + str(source_port)
    vis.create_network_source(source_port, source_name)
    vis.create_network_source_to_rotfile_pipe(
        pipe_name, rotfile, source_name)

    vis.start_pipe(pipe_name)

    # Packet byte-strings:
    rl = [str(p) for p in (
        Ether(src='00:30:48:9d:9b:a8', dst='ff:ff:ff:ff:ff:ff', type=2054)/ARP(hwdst='ff:ff:ff:ff:ff:ff', ptype=2048, hwtype=1, psrc='192.168.128.63', hwlen=6, plen=4, pdst='192.168.128.63', hwsrc='00:30:48:9d:9b:a8', op=1)/Padding(load='\xc0\xa8\x80\x0f\xe5op\x0c\xd5=\x0b\xff\x00\x0c\x1d\xa5j\xc0'),
        Ether(src='00:15:5d:81:87:0d', dst='ff:ff:ff:ff:ff:ff', type=2054)/ARP(hwdst='00:00:00:00:00:00', ptype=2048, hwtype=1, psrc='192.168.128.116', hwlen=6, plen=4, pdst='192.168.128.254', hwsrc='00:15:5d:81:87:0d', op=1)/Padding(load='\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'),
        Ether(src='2c:76:8a:d9:dc:8b', dst='01:00:5e:7f:ff:fa', type=2048)/IP(frag=0L, src='192.168.129.35', proto=17, tos=0, dst='239.255.255.250', chksum=10047, len=474, options=[], version=4L, flags=0L, ihl=5L, ttl=1, id=24334)/UDP(dport=1900, sport=1900, len=454, chksum=9378)/Raw(load='NOTIFY * HTTP/1.1\r\nHost:239.255.255.250:1900\r\nNT:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nNTS:ssdp:alive\r\nLocation:http://192.168.129.35:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
        Ether(src='2c:76:8a:d9:dc:8b', dst='33:33:00:00:00:0c', type=34525)/IPv6(nh=17, src='fe80::3161:e006:e572:74d7', dst='ff02::c', version=6L, hlim=1, plen=461, fl=0L, tc=0L)/UDP(dport=1900, sport=1900, len=461, chksum=52667)/Raw(load='NOTIFY * HTTP/1.1\r\nHost:[FF02::C]:1900\r\nNT:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nNTS:ssdp:alive\r\nLocation:http://[fe80::3161:e006:e572:74d7]:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
        Ether(src='2c:76:8a:d9:dc:8b', dst='01:00:5e:7f:ff:fa', type=2048)/IP(frag=0L, src='192.168.129.35', proto=17, tos=0, dst='239.255.255.250', chksum=10055, len=465, options=[], version=4L, flags=0L, ihl=5L, ttl=1, id=24335)/UDP(dport=1900, sport=1900, len=445, chksum=37880)/Raw(load='NOTIFY * HTTP/1.1\r\nHost:239.255.255.250:1900\r\nNT:upnp:rootdevice\r\nNTS:ssdp:alive\r\nLocation:http://192.168.129.35:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2::upnp:rootdevice\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
        Ether(src='2c:76:8a:d9:dc:8b', dst='33:33:00:00:00:0c', type=34525) /
        IPv6(nh=17, src='fe80::3161:e006:e572:74d7', dst='ff02::c', version=6L, hlim=1, plen=452, fl=0L, tc=0L) /
        UDP(dport=1900, sport=1900, len=452, chksum=35779) /
        Raw(load='NOTIFY * HTTP/1.1\r\nHost:[FF02::C]:1900\r\nNT:upnp:rootdevice\r\nNTS:ssdp:alive\r\nLocation:http://[fe80::3161:e006:e572:74d7]:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2::upnp:rootdevice\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
    )]

    nc_packets(rl, default_probe, source_port, 10)

    time.sleep(3)

    vis.get('admin/launch?script=rh&template=ncap-session')
    id = driver.find_element_by_xpath(
        "//tr[@class='row_container' and .//input[@value='" + pipe_name + "']]").get_attribute("id")
    vis.wait.until(
        lambda d: d.find_element_by_id("output_pkts_" + id).text != "0")
    vis.take_screenshot()

    vis.stop_pipe(pipe_name)

    assert driver.find_element_by_id("input_pkts_" + id).text == "60"
    assert driver.find_element_by_id("input_bytes_" + id).text == "22.56k"
    assert driver.find_element_by_id("output_pkts_" + id).text == "60"
    assert driver.find_element_by_id("output_bytes_" + id).text == "23.04k"

