#!/usr/bin/env python

from selenium.webdriver.support.ui import Select
import vision
import os
import pytest
import time
#
# Prameters:
# startign timestamp or 0 (the test will start with the smallest available value for the view)
# eg, datetime.datetime.strptime('Wed, 7 Aug 2013 06:54:00', '%a, %d %b %Y %H:%M:%S').strftime("%s")
ts_start = 0  # or 0
ts_step = 60

default_rotfile = 'latency1h'
default_vis_name = default_rotfile + "_vis"
default_tracefile = default_rotfile + '.erf'
default_probe = vision.default_probe

randomized = False  # True
#%%

@pytest.fixture(scope="session")
def capture_traffic(
    probe=default_probe,
    rotfile=default_rotfile,
    replay=False,
    tracefile=default_tracefile,
    clean=False):

    cli = vision.Cli(probe=probe, user="root", verbose=True)
    cli.capture_efrfile_from_netsource(
        rotfile=rotfile,
        tracefile=tracefile,
        replay=replay,
        clean=clean)

    return cli


@pytest.mark.parametrize("tool", ["TCP Latency",])
def test_multitool(capture_traffic, tool):

    cli = capture_traffic
    probe = cli.probe
    name = default_rotfile + " : " + tool
    rotfile = default_rotfile

    vis = vision.Vision(probe=probe)
    driver = vis.driver

    vis.create_network_source() # to make sure 'p7777' gets created

    vis.delete_visualization(name)
    vis.get_vision()

    driver.find_element_by_id("add_vis_link").click()
    driver.find_element_by_id("add_vis_name").clear()
    driver.find_element_by_id("add_vis_name").send_keys(name)

    Select(driver.find_element_by_name("add_vis_ids")
           ).select_by_visible_text(rotfile)

    driver.find_element_by_id("vis_type_7").click()
    driver.find_element_by_id("form_submit").click()
    #vis.wait_until_complete()
    el = vis.wait.until(
        lambda dr: dr.find_element_by_xpath("//button[contains(text(),'Tools')]")
    )

    view = vis.view_id

    el.click()
    driver.find_element_by_link_text("Add " + tool).click()
    driver.find_element_by_link_text("15 mins").click()
    driver.find_element_by_link_text("All Packets").click()
    driver.find_element_by_xpath("//button[@type='submit']").click()
    el = vis.wait.until(
        lambda dr: dr.find_element_by_css_selector("p.tool-progress.ng-isolate-scope > span.ng-scope")
    )

    assert el

#%%
if __name__ == "__main__":
    # user_traffic = os.environ.get("TRACEFILE")
    # if user_traffic is None: user_traffic = default_tracefile
    capture_traffic()
