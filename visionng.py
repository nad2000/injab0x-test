#!/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
"""
"""

import vision
import fixtures
from selenium.webdriver.support.ui import Select
import pytest
import time

vision_ng = "http://ux.et.endace.com:54621"
rotfile = fixtures.default_rotfile
probe = "192.168.132.139"
view_name = "FRAGMENTS"

@pytest.fixture(scope="session")
def visionng_traffic_breakdown_fragments():

    vis = vision.Vision(probe=probe, vision_ng=vision_ng)
    driver = vis.driver
    

    vis.get("/investigations")


    for view in [a.get_attribute("href") for a in driver.find_elements_by_xpath("//a[contains(., '" + view_name + "')]")]:
        vis.wait_element_by_xpath("//tr[td/a[contains(., '" + view_name + "')]]/td/a/i").click()


    
    vis.wait_element_by_link_text("Add Investigation").click()
    time.sleep(3)
    
    vis.wait_element_by_xpath("//div[@id='main-content']/div[3]/div/div/div/i").click()
    vis.wait_element_by_xpath("//input[@type='text']").clear()
    vis.wait_element_by_xpath("//input[@type='text']").send_keys(view_name)
    driver.find_element_by_css_selector("button.btn.btn-primary").click()
    time.sleep(3)

    vis.wait_element_by_xpath("(//button[@type='button'])[4]").click()
    vis.wait_element_by_xpath("//textarea[@type='text']").click()
    
    source_el = vis.wait_element_by_xpath("//a[contains(text(),'" + rotfile + "')]")
    source_el.click()
   
    nav_timerange_button = vis.wait_element_by_xpath("//div[contains(@class,'nav-timerange')]/button")
    nav_timerange_button.click()
    
    nav_timerange_list = vis.wait_element_by_xpath("//div[contains(@class, 'nav-timerange-div')]/a/span")
    nav_timerange_list.click()
    
    vis.wait_element_by_xpath("//a[contains(., 'All Packets')]").click()
    vis.wait_element_by_xpath("//button[@type='submit']").click()
    
    
    vis.wait_element_by_xpath("//div[@id='main-content']/div[3]/div/div[2]/div/form/div[4]/div/i").click()
    driver.find_element_by_xpath("//div[@id='main-content']/div[3]/div/div/div[2]/div[2]/button").click()
    driver.find_element_by_link_text("Add Traffic Breakdown").click()

    driver.find_element_by_xpath("(//button[@type='button'])[5]").click()
    driver.find_element_by_link_text("Directionless IP").click()
    driver.find_element_by_link_text("Directionless Port").click()
    driver.find_element_by_xpath("//div[@id='main-content']/div[3]/div/div[2]/div/form/div[4]/div[2]/div/table/tbody/tr/td[3]/ul/li[2]/div/div/div/textarea").click()
    driver.find_element_by_xpath("//div[@id='main-content']/div[3]/div/div[2]/div/form/div[4]/div[2]/div/table/tbody/tr/td[3]/ul/li[2]/div/div/div/textarea").clear()
    driver.find_element_by_xpath("//div[@id='main-content']/div[3]/div/div[2]/div/form/div[4]/div[2]/div/table/tbody/tr/td[3]/ul/li[2]/div/div/div/textarea").send_keys("icmp fragments, igmp fragments, hopopt fragments,")

    
    return vis

#%%

#vis = visionng_traffic_breakdown()

#%%




@pytest.mark.parametrize("breakdown_type, expected", [
    ("Data Source", 1),
    #"Application",
    #"IP Protocol",
    #"IP Version", 
    #"VLAN",
    #"MPLS",
    #"DSCP",
    #"Port",
    #"Source Port",
    #"Destination Port",
    #"Server Port",
    #"Client Port",
    #"Source IP Address",
    #"Destination IP Address",
    #"Server IP Address",
    #"Client IP Address",
    #"Source MAC",
    ("Destination MAC", 6)]) ### , "Destination Port", "Server Port", "Client Port"
def test_port_fragmet_filter_VNG901(visionng_traffic_breakdown_fragments, breakdown_type, expected):
    """
    VisionNG / VNG-901: Cannot filter 'fragmented' ports from filter bar or traffic breakdown tool
    """
    vis = visionng_traffic_breakdown_fragments
    driver = vis.driver

    traffic_breakdown_by = Select(vis.wait_element_by_xpath("//div[@class='tool-toolbar']/p[contains(@class, 'tool-select')]/select"))
    traffic_breakdown_by.select_by_visible_text(breakdown_type)
    #%%

    
    driver.find_element_by_xpath("//button[@type='submit']").click()
    
    #%%        

    chart_legend_table = vis.wait_element_by_xpath("//div[@class='tool-container']//table[contains(@class, 'chart-legend-table')]")
    rows = chart_legend_table.find_elements_by_xpath("tbody/tr")
    #%%

    assert len(rows) == expected
    #assert rows[0].text.strip() == u'ICMP 24 KB'
    #assert rows[1].text.strip() == u'IGMP 17 KB'
    #assert rows[2].text.strip() == u'HOPOPT 490 B'

#%%
    
def is_element_present(self, how, what):
    try: self.driver.find_element(by=how, value=what)
    except NoSuchElementException, e: return False
    return True
    
def is_alert_present(self):
    try: self.driver.switch_to_alert()
    except NoAlertPresentException, e: return False
    return True
    
def close_alert_and_get_its_text(self):
    try:
        alert = self.driver.switch_to_alert()
        alert_text = alert.text
        if self.accept_next_alert:
            alert.accept()
        else:
            alert.dismiss()
        return alert_text
    finally: self.accept_next_alert = True
