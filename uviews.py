#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import vision
import unittest
import os

default_probe = os.environ.get("TARGET_HOST")
default_start = 1387153028
default_end = 1387153148
default_pipe = 'uview'


class Granularity(vision.TestCase):

    def test_granularities(self):

        probe = default_probe
        self.probe = probe

        rotfile = 'test_sinlge_flow'
        self.rotfile = rotfile

        pipe = os.environ.get('PIPE', default_pipe)
        self.pipe = pipe

        cli = vision.Cli(probe=probe, verbose=True)

        # clean-up after previous runs:
        cli.stop_pipe(pipe)

        # set-up test:
        # Granularities in seconds (fractions)
        granularities = [
            30,
            10,
            1,
            0.1,
            0.07,
            0.05,
            0.02,
            0.015,
            0.01,
            0.005,
            0.001,
            0.0001,
            0.00001,
            0.000001,
        ]

        for g in granularities:
            print "*** Running pipe wiht granularity %f ***" % g
            cli.stop_pipe(pipe)
            cli.set_pipe_granularity(pipe, g)
            cli.start_pipe(pipe)
            cli.wait_for_pipe_to_finish(pipe)

if __name__ == "__main__":
    unittest.main()
