#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Common fixtures accross the tests

Created on Tue Jun  2 15:22:18 2015
@author: nad2000
"""

from selenium.webdriver.support.ui import Select
import vision
import os
import pytest
import re
import time
from scapy.all import Ether, IP, IPv6, ARP, UDP, Raw, Padding, TCP

#
# Prameters:
# startign timestamp or 0 (the test will start with the smallest available value for the view)
# eg, datetime.datetime.strptime('Wed, 7 Aug 2013 06:54:00', '%a, %d %b %Y %H:%M:%S').strftime("%s")
ts_start = 0  # or 0
ts_step = 60

default_rotfile = 'test_traffic_1h_v0.3'
default_vis_name = default_rotfile + "_vis"
default_tracefile = default_rotfile + '.erf'
default_probe = os.environ.get("TARGET_HOST")

randomized = False  # True
#%%
stats = []


def swap_src_dst(pkt):
    """Creates a packet in the reversed direction"""

    try:
        pkt.src, pkt.dst = pkt.dst, pkt.src
    except:
        pass

    try:
        pkt.sport, pkt.dport = pkt.dport, pkt.sport
    except:
        pass

    if IP in p:
        pkt[IP].src, pkt[IP].dst = pkt[IP].dst, pkt[IP].src

    if IPv6 in p:
        pkt[IPv6].src, pkt[IPv6].dst = pkt[IPv6].dst, pkt[IPv6].src

    return pkt


def injected_reversed(packets):
    """Adds a reversed copy (srouce and destination swapped) of the packed in the list"""
    for p in packets:
        yield p
        yield swap_src_dst(p.copy())

    return


@pytest.fixture(scope="session")
def capture_traffic_from_dag(
    probe=default_probe,
    rotfile="synthetic_telstra_nexus",
    replay=True,
    tracefile="synthetic_telstra_nexus.erf",
    clean=False):

    """
    Creates a rotfile and a pile from dag
    runs dagflood and captures.
    """

    cli = vision.Cli(probe=probe, user="root", verbose=True)
    cli.capture_traffic(rotfile=rotfile, replay=replay, clean=clean, traffic=tracefile,
                        interface='dagmod.3' if probe=='192.168.132.139' else None,
                        device={'192.168.132.139': 0, '192.168.132.111': 1}.get(probe, None))

    def fin():
        cli.close()

    return cli


@pytest.fixture(scope="session")
def capture_traffic(
    probe=default_probe,
    rotfile=default_rotfile,
    tracefile=default_tracefile,
    clean=False):
    """Capture traffic from a trace files via a netsource"""

    cli = vision.Cli(probe=probe, verbose=True)
    cli.capture_efrfile_from_netsource(
        rotfile=rotfile, tracefile=tracefile, clean=clean)

    def fin():
        print "### Finalizing ###"
        try:
            cli.delete_pipe(rotfile+'_pipe')
            cli.delete_rotfile(rotfile)
            cli.close()
        except:
            pass

    return cli


@pytest.fixture(scope="session")
def app_vis():
    """
    Create application breakdown with 4 top applications filtered-out
    """

    vis = create_app_vis()

    def fin():
        print "### Finalizing ###"
        try:
            vis.close_driver()
        except:
            pass

    return vis


@pytest.fixture(scope="session")
def uview_ot_vis():
    """
    Create application breakdown with 4 top applications filtered-out
    for uView tests
    """

    name = "uview_" + default_vis_name
    vis = create_app_vis(name=name)
    driver = vis.driver

    vis.get_vis(name)
    view = vis.view_id

    driver.find_element_by_id("vis_time_1hr_" + view).click()
    vis.wait_until_complete()

    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")
    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    vis.view_type = vision.TRAFFIC_BREAKDOWN_OVER_TIME

    def fin():
        print "### Finalizing ###"
        try:
            vis.close_driver()
        except:
            pass

    return vis


def create_app_vis(
        probe=default_probe,
        name=None,
        rotfile=default_rotfile,
        tracefile=default_tracefile,
        replay=True):
    """
    Create application breakdown with 4 top applications filtered-out

    name - name of the vision view
    rotfile - view over this rotfile (captured traffic)
    replay - capture with "replay" (for uView testing, you can replay=False)
    """

    if name is None:
        name = rotfile + "_vis"

    # we need to restart DBMGR because otherwise the uviews from previous run are reused
    cli = vision.Cli(probe=probe)
    cli.restart_service("dbmgr")
    time.sleep(5)
    cli.save_config()
    cli.close()

    capture_traffic(rotfile=rotfile, clean=False)
    vis = vision.Vision(probe=probe, rotfile=rotfile)

    vis.delete_visualization(name)

    vis.get_vision()
    driver = vis.driver

    driver.find_element_by_id("add_vis_link").click()
    driver.find_element_by_id("add_vis_name").clear()
    driver.find_element_by_id("add_vis_name").send_keys(name)

    select = Select(driver.find_element_by_name("add_vis_ids"))
    for o in select.options:
        if rotfile in o.text:
            select.select_by_visible_text(o.text)

    driver.find_element_by_id("vis_type_2").click()
    driver.find_element_by_id("form_submit").click()
    vis.wait_until_complete()

    view = vis.view_id

    # Change to 'Application':
    Select(driver.find_element_by_id("select_breakdown_types")
           ).select_by_visible_text("Application")
    driver.find_element_by_id("breakdown_num_results").clear()
    driver.find_element_by_id("breakdown_num_results").send_keys("10")

    driver.find_element_by_id("filters_apply").click()
    vis.wait_until_complete()

    # Filter out top applications:
    vis.check_select("http")
    vis.check_select("udp")
    #vis.check_select("ssl")
    vis.check_select("tcp")
    #vis.check_select("smtp")

    try:
        # Filter out selected:
        driver.find_element_by_id("filter_selected_e").click()
        vis.wait_until_complete()
    except:
        pass

    # Reduce time range to one min:
    driver.find_element_by_id("vis_time_1min_" + view).click()
    vis.wait_until_complete()

    return vis


test_packets = [str(p) for p in injected_reversed((
    Ether(src='00:30:48:9d:9b:a8', dst='ff:ff:ff:ff:ff:ff', type=2054)/ARP(hwdst='ff:ff:ff:ff:ff:ff', ptype=2048, hwtype=1, psrc='192.168.128.63', hwlen=6, plen=4, pdst='192.168.128.63', hwsrc='00:30:48:9d:9b:a8', op=1)/Padding(load='\xc0\xa8\x80\x0f\xe5op\x0c\xd5=\x0b\xff\x00\x0c\x1d\xa5j\xc0'),
    Ether(src='00:15:5d:81:87:0d', dst='ff:ff:ff:ff:ff:ff', type=2054)/ARP(hwdst='00:00:00:00:00:00', ptype=2048, hwtype=1, psrc='192.168.128.116', hwlen=6, plen=4, pdst='192.168.128.254', hwsrc='00:15:5d:81:87:0d', op=1)/Padding(load='\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'),

    Ether(src='2c:76:8a:d9:dc:8b', dst='01:00:5e:7f:ff:fa', type=2048) /
    IP(frag=0L, src='239.255.255.250', proto=77, tos=0, dst='192.168.129.35', chksum=10047, len=474, options=[], version=4L, flags=0L, ihl=5L, ttl=1, id=24334) /
    TCP(dport=888, sport=999) /
    Raw(load='NOTIFY * HTTP/1.1\r\nHost:239.255.255.250:1900\r\nNT:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nNTS:ssdp:alive\r\nLocation:http://192.168.129.35:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),

    Ether(src='2c:76:8a:d9:dc:8b', dst='01:00:5e:7f:ff:fa', type=2048)/IP(frag=0L, src='192.168.129.35', proto=17, tos=0, dst='239.255.255.250', chksum=10047, len=474, options=[], version=4L, flags=0L, ihl=5L, ttl=1, id=24334)/UDP(dport=1900, sport=1900, len=454, chksum=9378)/Raw(load='NOTIFY * HTTP/1.1\r\nHost:239.255.255.250:1900\r\nNT:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nNTS:ssdp:alive\r\nLocation:http://192.168.129.35:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
    Ether(src='2c:76:8a:d9:dc:8b', dst='33:33:00:00:00:0c', type=34525)/IPv6(nh=17, src='fe80::3161:e006:e572:74d7', dst='ff02::c', version=6L, hlim=1, plen=461, fl=0L, tc=0L)/UDP(dport=1900, sport=1900, len=461, chksum=52667)/Raw(load='NOTIFY * HTTP/1.1\r\nHost:[FF02::C]:1900\r\nNT:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nNTS:ssdp:alive\r\nLocation:http://[fe80::3161:e006:e572:74d7]:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
    Ether(src='2c:76:8a:d9:dc:8b', dst='01:00:5e:7f:ff:fa', type=2048)/IP(frag=0L, src='192.168.129.35', proto=17, tos=0, dst='239.255.255.250', chksum=10055, len=465, options=[], version=4L, flags=0L, ihl=5L, ttl=1, id=24335)/UDP(dport=1900, sport=1900, len=445, chksum=37880)/Raw(load='NOTIFY * HTTP/1.1\r\nHost:239.255.255.250:1900\r\nNT:upnp:rootdevice\r\nNTS:ssdp:alive\r\nLocation:http://192.168.129.35:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2::upnp:rootdevice\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
    Ether(src='2c:76:8a:d9:dc:8b', dst='33:33:00:00:00:0c', type=34525) /
    IPv6(nh=17, src='fe80::3161:e006:e572:74d7', dst='ff02::c', version=6L, hlim=1, plen=452, fl=0L, tc=0L) /
    UDP(dport=1900, sport=1900, len=452, chksum=35779) /
    Raw(load='NOTIFY * HTTP/1.1\r\nHost:[FF02::C]:1900\r\nNT:upnp:rootdevice\r\nNTS:ssdp:alive\r\nLocation:http://[fe80::3161:e006:e572:74d7]:2869/upnphost/udhisapi.dll?content=uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2\r\nUSN:uuid:e53349b4-bf1f-480e-8bb1-ffc001432fe2::upnp:rootdevice\r\nCache-Control:max-age=1800\r\nServer:Microsoft-Windows-NT/5.1 UPnP/1.0 UPnP-Device-Host/1.0\r\nOPT:"http://schemas.upnp.org/upnp/1/0/"; ns=01\r\n01-NLS:0225c9d02d7781324e7cfd3a5f6927c5\r\n\r\n'),
))]

@pytest.fixture(scope="session")
def build_data_uview(probe=default_probe, rotfile=default_rotfile):
    global test_packets

    if probe is None:
        probe = os.environ.get("TARGET_HOST", default_probe)

    if rotfile is None:
        rotfile = 'test_uview'

    ts0 = time.time() - 1000
    capture_packets(probe=probe, rotfile=rotfile, packets=test_packets, start_ts=ts0, delta=0.27)

    # Now generate microviews wiht different granularities:
    granularities = [
        30,
        10,
        1,
        0.1,
        0.07,
        0.05,
        0.02,
        0.015,
        0.01,
        0.005,
        0.001,
        0.0001,
        0.00001,
        0.000001,
    ]

    pipe = 'test_uview_pipe'

    cli = vision.Cli(probe=probe, verbose=True)

    cli.delete_pipe(pipe)

    cli.create_uview_pipe(name=pipe, source_rotfile=rotfile,
              from_ts=ts0, to_ts=ts0+300)

    for g in granularities:
        #print "*** Running pipe wiht granularity %f ***" % g
        cli.stop_pipe(pipe)
        cli.set_pipe_granularity(pipe, g)
        cli.start_pipe(pipe)
        cli.wait_for_pipe_to_finish(pipe)

    def fin():
        cli.close()

    return cli