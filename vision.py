from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import selenium.webdriver.support.ui as ui
from selenium.common.exceptions import NoSuchElementException
import unittest
import time
import re
import os
from collections import namedtuple
import socket
from scapy.all import *
import pg8000
import pxssh

#%%
default_probe = os.environ.get("TARGET_HOST")
#
# Prameters:
default_hub  = os.environ.get("HUB_ADDRESS", os.environ.get("SELENIUM_SERVER"))
if not default_hub and "SSH_CLIENT" in os.environ:
    default_hub = os.environ.get("SSH_CLIENT")
    if default_hub:
        default_hub = default_hub.split(" ")[0].strip()

default_hub_port = os.environ.get("HUB_PORT", os.environ.get("SELENIUM_PORT", 4444))
# startign timestamp or 0 (the test will start with the smallest available value for the view)
## eg, datetime.datetime.strptime('Wed, 7 Aug 2013 06:54:00', '%a, %d %b %Y %H:%M:%S').strftime("%s")
#
#
default_pipe = "synthetic_telstra_nexus"
default_netsource_port = 7777
default_rotfile = "synthetic_telstra_nexus"
default_tracefile = "synthetic_telstra_nexus.erf"
stats = []
view = ''


# View Type:
BANDWIDTH = 1
TRAFFIC_BREAKDOWN = 2
TRAFFIC_BREAKDOWN_OVER_TIME = 3
CONVERSATIONS = 4
TOP_TALKERS = 5


def ts2string(ts):
    """Converts Unix timestamp into Vision datetime representation"""
    ts_str = time.strftime('%Y/%m/%d %H:%M:%S', time.gmtime(int(ts)))
    frac = int(round(ts - int(ts), 3) * 1000)
    #ts_str += '.' + "%03i" % frac if frac != 0 else ".000"
    if frac != 0: ts_str += '.' + "%03i" % frac

    return ts_str


def netcat(hostname, port, content):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((hostname, port))
    s.sendall(content)
    s.shutdown(socket.SHUT_WR)
    while 1:
        data = s.recv(1024)
        if data == "":
            break
        print "Received:", repr(data)
    print "Connection closed."
    s.close()


def create_socket(host=default_probe, port=default_netsource_port):
    """Creates and opens a socket connected to specified host and port.
    If the host isn't specified, connects to the default probe.
    Default port: 7777"""
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    for _ in range(5):
        try:
            s.connect( (host , port))
            break
        except:
            time.sleep(1)
    return s


def write_erf(stream, packet, ts=None):

    """
    Sreams to the file a padded to 16 bytes TYPE_ETH erf record from scapy packet
    """

    if isinstance(packet, str):
        rr = packet
    else:
        rr = str(packet)

    if ts is None:
        ts = time.time()

    ts, frac = int(ts), int((ts - int(ts))*4294967296)

    # payload length
    pl_len = len(rr)
    len_rem = (18+pl_len) & 0x0f
    # padding length
    pd_len = 16-len_rem if len_rem != 0 else 0

    stream.write(struct.pack('<II', frac, ts))
    stream.write(
        struct.pack('>BBHHH', 0x2, 0x0, pl_len+18+pd_len, 0, pl_len)+'\0\0')
    stream.write(rr)
    stream.write('\0'*pd_len)
    stream.flush()


def nc_packets(packets, host=default_probe, port=default_netsource_port, times=1):
    """'netcat's multiple timest packets wrapped inot ERF records to
    specified prot of the probe under the test."""
    s = create_socket(host, port)
    f = s.makefile('w')
    for _ in xrange(times):
        for p in packets:
            write_erf(f,p)
    s.close()


def erfts(ts):
    """Converts timestamp decimal value with fraction inot ERF timestamp"""
    return (int(ts) << 32) | int((ts - int(ts)) * 4294967296)


def to_erf(packet, ts=None):
    """Creates padded to 16 bytes TYPE_ETH erf record from scapy packet"""

    if isinstance(packet, str):
        rr = packet
    else:
        rr = str(packet)

    if ts is None:
        ts = time.time()

    ts, frac = int(ts), int((ts - int(ts))*4294967296)

    # payload lenth
    pl_len = len(rr)
    len_rem = (18+pl_len) & 0x0f
    # padding lenght
    pd_len = 16-len_rem if len_rem != 0 else 0

    erf = ''.join([
        struct.pack('<II', frac, ts),
        struct.pack('>BBHHH', 0x2, 0x0, pl_len+18+pd_len, 0, pl_len)+'\0\0',
        rr + '\0' * pd_len])

    return erf


def capture_erffile_to_port(probe, source_port, erf_file):
    if erf_file[-4:] != ".bz2":
        cmd = "nc {0} {1} < {2}"
    else:
        cmd = "bzip2 -d -c {2} | nc {0} {1}"
    cmd = cmd.format(probe, source_port, erf_file)
    print >>sys.stderr, "*** Running: ", cmd
    return os.system(cmd)


class NetworkSource(object):

    def __init__(self, probe=default_probe,port=default_netsource_port):
        self.probe = probe
        self.__s = create_socket( host=probe,port=port)
        self.__f = self.__s.makefile('w')

        self._rotfile_db = None
        self._last_ts = None
        self._last_sent_at = None

    def write(self, packet, ts=None):
        if ts is None:
            if self._last_ts is None:
                ts = self._last_ts = time.time()
            elif self._last_sent_at is None:
                ts = self._last_ts
            else:
                ts = self._last_ts + (time.time() - self._last_sent_at)

        self._last_ts = ts
        write_erf(self.__f, packet, ts)
        self._last_sent_at = time.time()
        return ts

    def close(self):
        self.__s.close()


class MetaDb(object):

    _probedb = None
    _erfstoredb = None
    _dbuser = "postgres"

    def __init__(self, probe=default_probe,rotfile=default_rotfile):
        self.probe = probe
        self._rotfile = rotfile
        self._rotfile_db = None
        self._probe=probe

    def source_exists(self, name):
        cr = self.probedb.cursor()
        cr.execute("SELECT 1 FROM erfstores WHERE erfstore_name='%s'" % name)
        res = cr.rowcount > 0
        cr.close()
        return res

    @property
    def rotfile(self):
        return self._rotfile

    @rotfile.setter
    def rotfile(self, rotfile):
        if self._rotfile != rotfile:
            self._rotfile = rotfile
            if self._rotfile_db is not None:
                self._rotfile_db.close()
                self._rotfile_db = None

    @property
    def probedb(self):
        """Provides connection to the probe's meta DB.
        The access should be open in pg_hba.conf"""
        if MetaDb._probedb is None:
            MetaDb._probedb = pg8000.connect(
                host=self.probe,
                user=self._dbuser,
                database="probedb")
        return MetaDb._probedb

    @property
    def erfstoredb(self):
        """Provides connection to the erfstore template DB.
        The access should be open in pg_hba.conf"""
        if MetaDb._erfstoredb is None:
            MetaDb._erfstoredb = pg8000.connect(
                host=self.probe,
                user=self._dbuser,
                database="erfstoredb")
        return MetaDb._erfstoredb

    def ceanup_pgtap(self, db=None):
        if db is None:
            db = self.probedb
        try:
            db.execute("SELECT ut.shutdown_common()::text")
            db.commit()
        except:
            db.rollback()

    def run_pgtap_tests(self, db=None, schema='ut', pattern=None):

        if db is None:
            db = self.probedb

        cr = db.cursor()
        cr.execute("SET client_min_messages = notice")
        if pattern is None:
            cr.execute("SELECT runtests('%s'::name)" % schema)
        else:
            cr.execute("SELECT runtests('%s'::name,'%s')" % (schema,pattern))
        rows = [r[0] for r in cr.fetchall() if r[0] is not None and r[0].strip() != ""]
        cr.close()
        self.ceanup_pgtap(db)

        return rows

    def get_store_db(self, rotfile=default_rotfile):
        """Returs connection to the erfstore meta DB.
        The access should be open in pg_hba.conf"""

        if rotfile is None:
            rotfile = self.rotfile
        cr = self.probedb.cursor()
        cr.execute("SELECT erfstore_id FROM erfstores WHERE erfstore_name=%s",
                   (rotfile,))
        db_name = str(cr.fetchone()[0])
        cr.close()
        cn = pg8000.connect(
            host=self.probe, user=self._dbuser, database=db_name)

        return cn

    @property
    def rotfiledb(self):
        """The default rotfile meta data DB connection"""
        try:
            if self._rotfile_db is None:
                self._rotfile_db = self.get_store_db(self.rotfile)
        except:
            self._rotfile_db = self.get_store_db(self.rotfile)

        return self._rotfile_db

    def close(self):
        try:
            MetaDb._erfstoredb.close()
        except:
            pass
        try:
            MetaDb._probedb.close()
        except:
            pass
        try:
            self._rotfile_db.close()
        except:
            pass
        MetaDb._probedb = None
        MetaDb._erfstoredb = None
        self._rotfile_db = None

    @property
    def rotfiledb_uuid(self):
        """Returns rotfile GUID"""
        cr = self.probedb.cursor()
        cr.execute("SELECT id FROM erfstores WHERE erfstore_name=%s",
                   (self.rotfile,))
        guid = str(cr.fetchone()[0])
        return guid

    def query_iter(self, sql, args=(), size=100):
        """Return iterator over the rotfile DB query result set"""
        cr = self.rotfiledb.cursor()
        cr.execute(sql, args)
        while True:
            rows = cr.fetchmany(size)
            if not rows:
                break
            for r in rows:
                yield r
        cr.close()

    def query_all(self, sql, args=(), size=100):
        cr = self.rotfiledb.cursor()
        cr.execute(sql, args)
        rows = cr.fetchall()
        cr.close()
        return rows

    def query_one(self, sql, args=()):
        cr = self.rotfiledb.cursor()
        cr.execute(sql, args)
        row = cr.fetchone()
        cr.close()
        return row

    def query_scalar(self, sql, args=()):
        row = self.query_one(sql, args)
        return row[0] if row else None

    @property
    def erfstores(self):
        cr = self.probedb.cursor()
        cr.execute("""
                   SELECT id, erfstore_name, erfstore_id AS name, pg_database_size(erfstore_id) AS size FROM erfstores;
                   """)
        ErfstoreDataBase = namedtuple("ErfstoreDataBase", ("id", "erfstore_name", "name", "size"))
        return [ErfstoreDataBase._make(r) for r in cr.fetchall()]

    def get_counts(self):
        """Returns packet and byte counts from meta DB"""

        return self.query_one("""
        SELECT sum(pc), sum(bc) FROM (
            SELECT sum(packet_count_1+packet_count_2) AS pc, sum(byte_count_1+byte_count_2) AS bc
            FROM tcp_samples
            UNION ALL
            SELECT sum(packet_count_1+packet_count_2), sum(byte_count_1+byte_count_2)
            FROM nontcp_samples
            UNION ALL
            SELECT sum(packet_count_1+packet_count_2), sum(byte_count_1+byte_count_2)
            FROM long_flow_samples_600
            UNION ALL
            SELECT sum(mpc), sum(mbc) FROM (
                SELECT
                    max(packet_count_1+packet_count_2) mpc,
                    max(byte_count_1+byte_count_2) mbc
                FROM nonip_flow_samples
                GROUP BY flow_id) AS d
            UNION ALL
            SELECT sum(mpc), sum(mbc) FROM (
                SELECT
                    max(packet_count_1+packet_count_2) mpc,
                    max(byte_count_1+byte_count_2) mbc
                FROM long_flow_samples
                GROUP BY flow_id) AS d
            UNION ALL
            SELECT sum(mpc), sum(mbc) FROM (
                SELECT
                    max(packet_count_1+packet_count_2) mpc,
                    max(byte_count_1+byte_count_2) mbc
                FROM flow_samples
                GROUP BY flow_id) AS d
        ) AS stats
        """)

    @property
    def last_start_ts(self):
        """Returns last capturing sessions start timestamp.
        If thre were multiple capturing sessions and there is "holes" in the data
        between them, it will return the very last session start time stamp."""

        cr = self.rotfiledb.cursor()
        cr.execute(
            """WITH d as (SELECT timestamp, lag(timestamp) OVER () AS ts  FROM bandwidth_stats )
            SELECT max(timestamp) FROM d WHERE timestamp-1 > ts OR ts IS NULL""")
        if cr.rowcount==0:
            ts = None
        else:
            ts = cr.fetchone()[0]
        cr.close()
        return ts


class Cli(object):
    """Pexpect wrapper for directly interactin with CLI via SSH"""

    # Pipe IDs cash:
    __pipe_ids = {}

    @property
    def ssh(self):
        try:
            ssh = self._ssh
        except:
            ssh = self.connect()
        return ssh

    def __init__(self, probe=default_probe, verbose=False, user="admin"):
        """Connects to the CLI and enter configuration mode."""
        self.probe = probe
        self.verbose = verbose
        self.user = user

    def connect(self):
        TERM = "dumb"
        os.environ["TERM"] = TERM
        ssh = pxssh.pxssh()
        ssh.options = dict(StrictHostKeyChecking="no", UserKnownHostsFile="/dev/null")

        if self.verbose:
            ssh.logfile = sys.stdout

        if self.user == "admin":
            ssh.login(
                self.probe, self.user, 'injab0xn',
                original_prompt='[>#]', auto_prompt_reset=False,
                terminal_type=TERM)
            ssh.sendline('enable')
            ssh.PROMPT = '# '
            ssh.prompt()
            ssh.sendline('configure t')
            ssh.prompt()

        else:  # user == "root"
            ssh.login(
                self.probe, self.user, password=None,
                original_prompt='[>#]', auto_prompt_reset=False,
                terminal_type=TERM)
            ssh.PROMPT = '# '
            ssh.sendline("cli -m config")
            ssh.prompt()

        ssh.sendline("terminal length 999")
        ssh.prompt()
        ssh.prompt(0.1)
        self._ssh = ssh
        return ssh

    def execute(self, command, timeout=5):
        self.ssh.sendline()
        self.ssh.prompt(1)

        self.ssh.sendline(command)
        time.sleep(0.5)
        self.ssh.prompt(timeout)

    def save_config(self):
        self.execute('write mem')

    def configure(self, command):
        """Change configuration executing a configuration command"""
        self.execute(command)
        self.save_config()

    def create_rotfile(self, name=None, size=64, vision_enabled=True, metadb_size_limit=100):
        if name is None:
            name = Cli.default_rotfile
        self.configure('erfstream rotation-file %s size-limit %d  vision %s' %
                       (name, size, 'enable vision size-limit %d' %
                        metadb_size_limit if vision_enabled else 'disable'))

    def create_netsource(self, port=default_netsource_port, name='p7777'):
        self.execute('erfstream netsource %s listen-port %d' % (name, port))

    def create_netsource_pipe(self, name=None, app_detect=True, rotfile=default_rotfile, netsource='p7777'):
        if name is None:
            name = rotfile+'_pipe'

        self.configure(
            'erfstream pipe %s source network-source %s %s enable sink rotation-file %s' %
            (name, netsource,"application-detection" if app_detect else "", rotfile))

    def create_nic_pipe(self, name=None, app_detect=True, rotfile=default_rotfile, nic='eth0'):
        if name is None:
            name = rotfile+'_pipe'

        self.configure(
            'erfstream pipe %s source nic %s %s enable sink rotation-file %s' %
            (name, nic,"application-detection" if app_detect else "", rotfile))

    def rotfile_exists(self, rotfile):
        #self.execute("show erfstream rotation-file name %s" % rotfile)
        self.ssh.sendline("show erfstream rotation-file name %s" % rotfile)
        self.ssh.prompt(1)
        
        return ('RotationFile %s' % rotfile) in self.ssh.before \
            and not ('not found' in self.ssh.before)

    def create_uview_pipe(self, name=None, source_rotfile=None,
                          sink_rotfile=None, app_detect=True,
                          from_ts=None, to_ts=None):
        if sink_rotfile is None:
            sink_rotfile = 'zero'
        if source_rotfile is None:
            source_rotfile = Cli.default_rotfile
        if name is None:
            name = source_rotfile+'_pipe'

        if not self.rotfile_exists(sink_rotfile):
            self.create_rotfile(name=sink_rotfile, size=0)

        self.configure(
            'erfstream pipe %s source rotation-file %s %s enable sink rotation-file %s'
            % (name, source_rotfile, "application-detection" if app_detect else "", sink_rotfile))

        id = self.get_pipe_id(name)

        # Change pipe type to uview pipe:
        self.execute(
            'internal set modify - /endace/captured/config/pipe/%d/data_sink/1/type value uint8 17' % id)

        if from_ts is not None:
            self.execute(
                'internal set modify - /endace/captured/config/pipe/%d/time_start value uint64 %d' % (id, erfts(from_ts)))

        if to_ts is not None:
            self.execute(
                'internal set modify - /endace/captured/config/pipe/%d/time_end value uint64 %d' % (id, erfts(to_ts)))

    def get_pipe_id(self, name=None):
        if name is None:
            name = Cli.default_pipe

        if name not in Cli.__pipe_ids:
            ssh = self.ssh
            ssh.sendline()
            ssh.prompt()
            ssh.sendline("internal query iterate subtree /endace/captured/config/pipe")
            ssh.expect("/endace/captured/config/pipe/[0-9]+/name = %s" % name)

            match = re.search(
                "/endace/captured/config/pipe/([0-9]*)/name = %s" % name,
                ssh.after)
            Cli.__pipe_ids[name] = int(match.group(1))
            ssh.prompt()

        return Cli.__pipe_ids[name]

    @property
    def dags(self):

        dag_re = re.compile(
            "(?:DAG interface ([a-z.0-9]*).*Admin status:\s*(\w*).*Interface type:\s*(\w*))",
            re.M | re.S)

        self.execute("show daginterfaces brief")
        # hack to deal with long outputs
        if len(self.ssh.before) < 100:
            self.ssh.prompt()

        lines = self.ssh.before.split("\r\n\r\n") if "\r\n\r\n" in self.ssh.before else self.ssh.before.split("\n\n")
        return [m.groups() for m in (dag_re.search(dag) for dag in lines) if m]

    def set_pipe_granularity(self, name=None, granularity=1.0):
        """
        Set capturing granularity in seconds (second fractions)
        """
        if name is None:
            name = Cli.default_pipe

        id = self.get_pipe_id(name)

        # Change pipe type to uview pipe:
        self.execute(
            'internal set modify - /endace/captured/config/pipe/%d/data_sink/1/type value uint8 17' % id)

        self.execute(
            'internal set modify - /endace/captured/config/pipe/%d/vision_granularity value uint64 %d' \
            % (id, int(round(granularity * 4294967295)))
        )

    def start_pipe(self, name=None):
        if name is None:
            name = Cli.default_pipe
        self.execute('erfstream pipe %s enable' % name)
        time.sleep(3)

    def stop_pipe(self, name=None):
        if name is None:
            name = Cli.default_pipe
        self.execute('no erfstream pipe %s enable' % name)
        time.sleep(3)

    def wait_for_pipe_to_finish(self, name=None):
        """
        Wait for a pipe to finish and stop it
        """
        if name is None:
            name = Cli.default_pipe

        ssh = self.ssh
        while True:
            ssh.sendline('show erfstream pipe name %s' % name)
            ssh.prompt()
            match = re.search('Running:\s+(\w+)', ssh.before)
            if match:
                status = match.group(1)
                if status is not None and status != 'running':
                    break
            time.sleep(10)

        self.stop_pipe(name)

    def delete_netsource(self, port=default_netsource_port, name=None):
        if name is None:
            name = 'p'+str(port)
        self.configure('no erfstream netsource %s' % name)

    def delete_rotfile(self, name=None):
        if name is None:
            name = Cli.default_rotfile

        # to find a pipe that is connected to the rotfile
        pipe_re = re.compile(r"^(\S*).*%s" % name, re.M | re.S)

        self.execute("show erfstream pip")
        if len(self.ssh.before) < 40:
            self.ssh.prompt()
        for p in self.ssh.before.split("Pipe "):
            g = pipe_re.search(p)
            if g:
                self.delete_pipe(g.group(1))

        self.configure('no erfstream rotation-file %s' % name)

    def delete_pipe(self, name=None):
        if name is None:
            name = Cli.default_pipe
        self.configure('no erfstream pipe %s' % name)

    def capture_traffic(self,
            rotfile=default_rotfile,
            app_detect=True,
            replay=True,
            traffic=default_tracefile,
            interface=None,
            device=1,
            repeats=1,
            clean=False):
        """
        Creates a rotfile and a pile from dag
        runs dagflood and captures.
        NB: assumes /data/traffic exists!
        """


        if device is None:

            # get to shell:
            self.save_config()
            self.execute("quit")
            self.execute("daginf")
            devs = [dict(re.findall(r"(model|id|card|bus id)\ *([^\ ].*)\r\n", d))
                    for d in self.ssh.before.split("\r\n\r\n") if "vDAG" not in d]
            self.execute("cli -m config")
                
            if not devs:
                device = 1
            else:
                device = devs[0]["id"]

        self.execute("show erfstream rotation-file name " + rotfile)
        self.ssh.prompt()

        if self.rotfile_exists(rotfile):
            # if the rotfile already exists and we don't require a clean capture
            # double-check if any data was captured:
            db = MetaDb(self.probe, rotfile)
            last_start_ts = db.last_start_ts
            db.close()
            if last_start_ts is not None:
                return

        # otherwise we delete and create the rotfile cleanly
        pipe = rotfile + "_pipe"
        self.delete_pipe(pipe)

        self.delete_rotfile(rotfile)
        self.create_rotfile(name=rotfile)

        if interface is None:
            dags = set([d.strip()[:-2] for (d, status, model) in  self.dags if status == "up" and model=='10G_lan'])
        else:
            dags = [interface] if type(interface) is str else interface

        self.execute("erfstream pipe " + pipe + " " +
            ' '.join( "source dag " + name  for name in dags))

        self.execute(
            "erfstream pipe %s %s enable sink rotation-file %s" %
            (pipe, "application-detection" if app_detect else "", rotfile))

        self.start_pipe(pipe)

        # get to shell:
        self.save_config()
        self.execute("quit")
        #cli.execute("daginf  -d1")
        self.execute("dagconfig -d{0} nonic eql".format(device))
        # import pdb; pdb.set_trace()
        self.execute("dagflood --no-cache -f /data/{0} -d{1} -c{2}".format(traffic, device, repeats) + (" -R" if replay else ""),
                     timeout=120)

        # back to cli
        self.execute("cli -m config")

        self.stop_pipe(pipe)
        # restart CPD in order to fush all samples into DB
        self.restart_service("captured")
        self.save_config()

        return self

    def capture_efrfile_from_netsource(
            self, rotfile=None, tracefile=None, clean=False, port=7777):

        """
        Capture ERF file from test server
        """

        if not clean and self.rotfile_exists(rotfile):
            return

        self.delete_pipe(rotfile+'_pipe')
        self.delete_rotfile(rotfile)

        self.create_netsource(port=port, name='p%i' % port)
        self.create_rotfile(name=rotfile)
        self.create_netsource_pipe(name=rotfile+'_pipe', rotfile=rotfile, netsource='p%i' % port)
        self.start_pipe(name=rotfile+'_pipe')
        self.save_config()
        capture_erffile_to_port(self.probe, port, "/dag/design/" + tracefile + ".bz2" if not tracefile.startswith('/') else tracefile)
        self.stop_pipe(name=rotfile+'_pipe')
        #cli.capture_traffic(rotfile=rotfile, replay=replay, clean=clean, traffic=tracefile)

    def restart_service(self, process_name):
        self.execute("pm process %s restart" % process_name)
        time.sleep(5)
        return self

    def close(self):
        self.ssh.sendline('exit')
        self.ssh.sendline('exit')
        self.ssh.close()


class TestCase(unittest.TestCase):
    """
    Vision common TestCase encapsulates all commonly used GUI functions.
    """

    def __init__(self,  methodName='runTest'):

        super(TestCase, self).__init__(methodName)
        self.verificationErrors = []


class Vision(object):
    """
    Vision - API for interaction with Vison via Webdrive
    """

    _driver = None
    _wait = None
    _probe = None
    _probedb = None
    # Virtural display:
    _display = None
    # by default browser is on
    _headless = False
    _auto_screenshot = False
    _screenshot_location = './screenshots_' \
        + time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime())

    _dbuser = "postgres"
    _db = None

    resources_url = "admin/launch?script=rh&template=nres-erffile"
    data_pipes_url = "admin/launch?script=rh&template=ncap-session"

    def __init__(self, probe=default_probe, view_name=None, rotfile=None, vision_ng=None):

        self._driver = None
        self._probe = probe
        self.rotfile = rotfile

        if vision_ng is not None:
            self.base_url = vision_ng
            self.get("/")
            
            driver = self.driver
            time.sleep(3)

            if driver.current_url.endswith("/"):

                self.wait_element_by_xpath("//input[@type='text']").click()
                self.wait_element_by_xpath("//input[@type='text']").clear()
                self.wait_element_by_xpath("//input[@type='text']").send_keys(probe)
                self.wait_element_by_xpath("(//input[@type='text'])[2]").click()
                self.wait_element_by_xpath("(//input[@type='text'])[2]").clear()
                self.wait_element_by_xpath("(//input[@type='text'])[2]").send_keys("admin")
                self.wait_element_by_xpath("//input[@type='password']").click()
                self.wait_element_by_xpath("//input[@type='password']").clear()
                self.wait_element_by_xpath("//input[@type='password']").send_keys("injab0xn")
                self.wait_element_by_xpath("//button[contains(., 'Connect')]").click()
                time.sleep(1)
                self.wait_element_by_xpath("//li[@class='hostname']")
                
        else:
            self.base_url = "http://{0}/".format(self.probe)
            self.accept_next_alert = True
            self.login()
            if view_name is not None:
                self.get_vis(view_name)
            

    def __del__(self):
        try:
            self.close_driver()
        except:
            pass

    def close_driver(self):

        try:
            driver = self.driver
            if driver:
                driver.close()
                driver.quit()
                driver = None
                self._driver = None
        except:
            pass

    @property
    def auto_screenshot(self):
        return self._auto_screenshot

    @auto_screenshot.setter
    def auto_screenshot(self, value):
        self._auto_screenshot = value

    @property
    def probe(self):
        if self._probe is None:
            self._probe = default_probe
            if self._probe is None or self._probe == '':
                self._probe = os.environ.get("TARGET_HOST")
        return self._probe

    @probe.setter
    def probe(self, value):
        self._probe = value

    @property
    def rotfile(self):
        try:
            return self._rotfile
        except:
            return None

    @rotfile.setter
    def rotfile(self, value):
        self._rotfile = value
        self.db.rotfile = value

    def take_screenshot(self):

        location = self._screenshot_location

        if not os.path.exists(location):
            os.mkdir(location)
        try:
            self._screenshot_number += 1
        except:
            self._screenshot_number = 1

        png = self.driver.get_screenshot_as_png()
        with open(os.path.join(
                location,
                "%05d.png" % self._screenshot_number), "wb") as f:
            f.write(png)

        #self.driver.save_screenshot(
        #    os.path.join(
        #        location,
        #        "%05d.png" % self._screenshot_number))

    @property
    def driver(self):
        if self._driver is None:

            if Vision._headless and Vision._display is None:
                from pyvirtualdisplay import Display
                #self._display = Display(visible=0, size=(800, 600))
                Vision._display = Display(visible=0, size=(1024, 768))
                Vision._display.start()

            try:
                self._driver = webdriver.Firefox()
                #self._driver = webdriver.Chrome()
            except:
                # remote web-drive:
                # TODO: set-up remote Selenium server:
                # run remotely:
                # java -jar selenium-server-standalone-2.x.jar -trustAllSSLCertificates
                url = "http://{0}:{1}/wd/hub".format(default_hub, default_hub_port)
                self._driver = webdriver.Remote(
                    command_executor=url,
                    desired_capabilities=DesiredCapabilities.FIREFOX)

            self._driver.implicitly_wait(10)
            Vision._wait = ui.WebDriverWait(self._driver, 900)

        return self._driver

    @property
    def headless(self):
        """Show or not the browser"""
        return Vision._headless

    @headless.setter
    def headless(self, value):
        """NB! Headless test should be set before start of the testing"""
        Vision._headless = value

    @property
    def wait(self):
        """WebDrive "wait" object to issue "waiting"
        """
        return Vision._wait

    @property
    def db(self):
        if self._db is None:
            self._db = MetaDb(probe=self.probe)
            self._db.rotfile = self.rotfile
        return self._db

    @property
    def probedb(self):
        """Provides connection to the probe's meta DB.
        The access should be open in pg_hba.conf"""
        return self.db.probedb

    def get_store_db(self, rotfile=None):
        """Returs connection to the erfstore meta DB.
        The access should be open in pg_hba.conf"""
        return self.db.get_store_db(rotfile)

    @property
    def rotfiledb(self):
        """The default rotfile meta data DB connection"""
        return self.db.rotfiledb

    @property
    def rotfiledb_uuid(self):
        """Returns rotfile GUID"""
        return self.db.rotfiledb_uuid

    def query_iter(self, sql, args=(), size=100):
        """Return iterator over the rotfile DB query result set"""
        return self.db.query_iter(sql, args, size)

    def query_all(self, sql, args=(), size=100):
        return self.db.query_all(sql, args, size)

    def query_one(self, sql, args=()):
        return self.db.query_one(sql, args)

    def query_scalar(self, sql, args=()):
        return self.db.query_scalar(sql, args)

    @property
    def last_start_ts(self):
        """Returns last capturing sessions start timestamp.
        If thre were multiple capturing sessions and there is "holes" in the data
        between them, it will return the very last session start time stamp."""

        return self.db.last_start_ts

    @property
    def data_time_range(self):
        """Detect vision view time-range from view content
        """
        ts_min_s, ts_max_s = map(
            int,
            re.search("var ts_min_s\t= ([0-9]+);.*var ts_max_s\t= ([0-9]+);", self.driver.page_source, re.S|re.M).groups())
        return (ts_min_s, ts_max_s)

    @property
    def query_time_range(self):
        """Current query time range"""
        # TODO
        return (None, None)

    @query_time_range.setter
    def query_time_range(self, value):
        """Sets qeury time-range

        value - start and end time stamp tuple
        """
        driver = self.driver
        view = self.view_id
        start, end = map(ts2string, value)

        driver.find_element_by_id("vis_time_link_" + view).click()
        driver.find_element_by_id("pop_time_from_" + view).click()
        driver.find_element_by_id("pop_time_from_" + view).clear()
        driver.find_element_by_id("pop_time_from_" + view).send_keys(start)
        driver.find_element_by_id("pop_time_to_" + view).click()
        driver.find_element_by_id("pop_time_to_" + view).clear()
        driver.find_element_by_id("pop_time_to_" + view).send_keys(end)
        driver.find_element_by_id("selection_button_" + view).click()
        self.wait_until_complete()


    def save(self):
        """
        Saves configuration changes made from the web.
        """
        try:
            self.driver.find_element_by_name("save").click()
        except:
            pass

    def get(self, url):
        #import pdb
        #pdb.set_trace()

        try:
            previous_url = self.driver.current_url
        except:
            previous_url = None

        if previous_url is None or not url in previous_url:
            if self.auto_screenshot:
                self.take_screenshot()
            if not url.startswith('http'):
                url = self.base_url + url
            #print "****", url
            self.driver.get(url)
        return previous_url

    def get_vis(self, vis_name):

        previous_url = self.get("vision/visualizations")
        # a way of dealing with a generated content
        el = self.wait.until(
            lambda dr: dr.find_element_by_xpath("(//a[contains(text(),'" + vis_name + "')])[1]")
        )
        self.get(el.get_attribute("href"))
        self.wait_until_complete()
        return previous_url

    _view_type = None
    @property
    def view_type(self):
        # TODO:
        return self._view_type

    @view_type.setter
    def view_type(self, view_type):
        """Set Vision View type
        """
        if self._view_type == view_type:
            return

        driver = self.driver
        el = driver.find_element_by_id("configuration_vis_types")
        if  type(view_type) is str:
            el = el.find_element_by_xpath("//label[contains(text(), '%s')]" %s)
            el.find_element_by_tag_name("input").click()
        else:
            el.find_element_by_id("config_vis_type_%d" % view_type).click()

        self.driver.find_element_by_id("update_vis").click()
        self.wait_until_complete()
        self._view_type = view_type

    def create_socket(self, host=default_probe, port=default_netsource_port):
        """Creates and opens a socket connected to specified host and port.
        If the host isn't specified, connects to the current probe.
        Default port: 7777"""
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        for _ in range(5):
            try:
                s.connect((host, port))
                break
            except:
                time.sleep(1)
        return s

    @property
    def view_id(self):
        """ Detects the current Vision view ID """
        return re.search("view/(\d*).*$", self.driver.current_url).group(1)

    def change_pipe(self, pipe_name, cmd):
        "Change pipe status."

        driver = self.driver
        previous_url = self.get(self.data_pipes_url)

        els = driver.find_elements_by_xpath("//tr[@class='row_container' and .//input[@value='" + pipe_name + "']]")
        if els:

            id = els[0].get_attribute("id")

            if (cmd == 'start' and driver.find_elements_by_xpath("//div[@id='status_" + id + "']//input[@value='Start']") ) \
                or (cmd == 'stop' and driver.find_elements_by_xpath("//div[@id='status_" + id + "']//input[@value='Stop']")):

                try:
                    driver.find_element_by_xpath(
                        "//div[@id='status_" + id + "']//input[@value='" + cmd.capitalize() + "']").click()
                    self.save()
                    time.sleep(3)
                    self.wait.until(lambda d: d.find_element_by_id(
                        "status_text_" + id).text == ('Running' if cmd == 'start' else 'Stopped'))
                except:
                    pass

        self.return_to(previous_url)

    def stop_pipe(self, pipe_name):
        self.change_pipe(pipe_name, 'stop')

    def start_pipe(self, pipe_name):
        self.change_pipe(pipe_name, 'start')

    def remove_pipe(self, pipe_name):

        driver = self.driver

        previous_url = self.get(self.data_pipes_url)

        # Remove pipe:
        row_path = "//tr[@class='row_container' and .//input[@value='" + pipe_name + "']]"
        if driver.find_element_by_xpath(row_path):
            self.stop_pipe(pipe_name)
            id = driver.find_element_by_xpath(row_path).get_attribute("id")
            driver.find_element_by_id("row_" + id).click()
            driver.find_element_by_xpath("//input[@value='Remove']").click()
            self.accept_alert()
            self.save()

        self.return_to(previous_url)

    def remove_rotfile_pipes(self, rotfile):
        """
        Remove all pipes connected to the rotfile
        """

        driver = self.driver
        previous_url = self.get(self.data_pipes_url)

        lst = [el.get_attribute('value') for el in driver.find_elements_by_xpath(
            "//tr[@class='row_data']/td/table/tbody[.//input[@value='" + rotfile + "' and @title='" + rotfile + "']]/tr[1]/td[2]/div[1]/input[1]")]
        for pipe_name in lst:
	    self.remove_pipe(pipe_name)

        self.return_to(previous_url)

    def remove_rotfile(self, rotfile):
#        import pdb; pdb.set_trace()
        driver = self.driver
        previous_url = self.get(self.resources_url)

        if driver.find_elements_by_xpath("//tr[@class='erfstore_row_container' and .//input[@value='" + rotfile + "']]"):

            self.remove_rotfile_pipes(rotfile)
            try:
                id = driver.find_element_by_xpath(
                    "//tr[@class='erfstore_row_container' and .//input[@value='" + rotfile + "']]").get_attribute("id")

                driver.find_element_by_id("row_" + id).click()
                driver.find_element_by_id("btn_remove").click()
                self.accept_alert()
                self.save()
            except:
                self.take_screenshot()
                pass

        self.return_to(previous_url)

    def return_to(self, url):
#        import pdb; pdb.set_trace()

        #if url !=  self.driver.current_url:
            if self.auto_screenshot:
                self.take_screenshot()
            self.driver.get(url)

    def delete_visualization(self, vis_name):

        previous_url = self.get("vision/visualizations")

        while True:
            try:
                self.driver.find_element_by_xpath(
                    "//table[@id='DataTables_Table_0']/tbody/tr[contains(.,'{0}')]/td/a".format(vis_name)).click()
                self.driver.find_element_by_name("delete_confirm").click()
            except:
                break
        self.return_to(previous_url)

    def delete_all_visualization(self):

        previous_url = self.get("vision/visualizations")

        while True:
            try:
                self.driver.find_element_by_xpath("//table[@id='DataTables_Table_0']/tbody/tr/td/a[@alt='delete']").click()
                self.driver.find_element_by_name("delete_confirm").click()
            except:
                break
        self.return_to(previous_url)

    def login(self):
        """ Log-in into the probe """

        try:
            if self.__logged_in:
                return
        except:
            pass

        self.get("admin/launch?script=rh&template=login")

        if self.driver.find_elements_by_id("user_id"):
            self.driver.find_element_by_id("user_id").clear()
            self.driver.find_element_by_id("user_id").send_keys("admin")
            self.driver.find_element_by_id("user_password").clear()
            self.driver.find_element_by_id(
                "user_password").send_keys("injab0xn")
            self.driver.find_element_by_id("login_submit").click()

        self.__logged_in = True

    def logout(self):
        self.driver.find_element_by_id("head_site_logo").click()
        self.driver.find_element_by_id("head_logout_link").click()

        self.__logged_in = False

    def rotfile_exists(self, rotfile):
        import pdb; pdb.set_trace()
        driver = self.driver
        previous_url = self.get(self.resources_url)

        res = driver.find_elements_by_xpath("//input[@value='" + rotfile + "' and @name='" + rotfile + "']")

        self.return_to(previous_url)
        return res

    def create_rotfile(self, name=default_rotfile, rotfile_size=0, metadb_size_limit=100, ret_time_days=300):
        element = None
        driver = self.driver
        previous_url=self.get(self.resources_url)
        driver.find_element_by_id("btn_add").click()
        driver.find_element_by_id("name").send_keys(name)
        element = driver.find_element_by_id("user_size_limit_gb")
        if element is not None:
            element.clear()
            element.send_keys(rotfile_size)
        else:
            # backward commpatibility with 5.1.1:
            driver.find_element_by_name("f_size").send_keys(rotfile_size)
        element = driver.find_element_by_id("ret_time_vision")
        if element.is_enabled() is not True:
	    # compatibility between 5.1.x and 5.2.x
            driver.find_element_by_id("vision").click()
        element.clear()
        element.send_keys(ret_time_days)
        element = driver.find_element_by_id("metadb_size_limit_unit")
        if element.is_displayed():
            Select(element).select_by_visible_text('GB')
        element = driver.find_element_by_id("metadb_size_limit")
        element.clear()
        element.send_keys(metadb_size_limit)
        driver.find_element_by_css_selector("input.btn_save").click()
        self.save()
        self.return_to(previous_url)

    def create_network_source(self, port=default_netsource_port, name=None):
        # Create network source:

        driver = self.driver
        previous_url = self.get('admin/launch?script=rh&template=nres-netport')

        source_name = 'p' + str(port) if not name else name

        if not source_name in driver.page_source:
            driver.find_element_by_id("btn_add").click()
            driver.find_element_by_id("name").send_keys(source_name)
            driver.find_element_by_id("listen_port").send_keys(port)
            driver.find_element_by_css_selector("input.btn_save").click()
            self.save()

        self.return_to(previous_url)

    def create_network_source_to_rotfile_pipe(self, pipe_name, rotfile_name=None, source_name='p7777'):

        driver = self.driver
        previous_url = self.get(self.data_pipes_url)

        rotfile = pipe_name if not rotfile_name else rotfile_name

        driver.find_element_by_id("btn_add").click()
        driver.find_element_by_id("name").send_keys(pipe_name)

        Select(driver.find_element_by_xpath(
            "//div[@id='div-session-rw-data_src-type-0']/select")).select_by_visible_text("NetworkSource")
        Select(driver.find_element_by_xpath(
            "//div[@id='div-session-rw-data_src-netport-0']/select")).select_by_visible_text(source_name)

        Select(driver.find_element_by_xpath(
            "//div[@id='div-session-rw-data_sink-type-0']/select")).select_by_visible_text("RotationFile")
        Select(driver.find_element_by_xpath(
            "//div[@id='div-session-rw-data_sink-erfstore-0']/select")).select_by_visible_text(rotfile)

        element = driver.find_element_by_id("applicationdetection_enabled")
        if element.is_displayed():
            element.click()
        driver.find_element_by_id("pipe_add_save_0").click()
        self.save()

        self.return_to(previous_url)

    def capture_erffile_to_port(self, source_port, erf_file):
        return capture_erffile_to_port(self.probe, source_port, erf_file)

    def check_select(self, code):

        try:
            if type(code) == int:
                id = "check_select_" + str(code)
                if id in self.driver.page_source:
                    self.driver.find_element_by_id(id).click()

            else:
                self.driver.find_element_by_xpath(
                    "//div[@id='highchart_vis_%s_table']/table/tbody/tr[td[3]='%s']/td[1]/input" % \
                    (self.view_id, code)).click()
        except:
            # TODO: log warning
            pass


    _breakdown_type = None
    @property
    def breakdown_type(self):
        return self._breakdown_type

    @breakdown_type.setter
    def breakdown_type(self, val):
        if self._breakdown_type == val:
            return
        drv = self.driver
        self._breakdown_type = val
        Select(drv.find_element_by_id("select_breakdown_types")
               ).select_by_visible_text(val)
        drv.find_element_by_id("breakdown_num_results").clear()
        drv.find_element_by_id("breakdown_num_results").send_keys("10")
        drv.find_element_by_id("filters_apply").click()
        self.wait_until_complete()

    def filter_in_selected(self):
        try:
            # Filter in selected:
            self.driver.find_element_by_id("filter_selected_i").click()
            self.wait_until_complete()
        except:
            pass

    def filter_out_selected(self):
        try:
            # Filter in selected:
            self.driver.find_element_by_id("filter_selected_e").click()
            self.wait_until_complete()
        except:
            pass

    def add_filter(self, filter_by, filter_type, values):
        """ unfinished """

        driver = self.driver
        driver.find_element_by_link_text("Add filter").click()
        Select(driver.find_element_by_id("add_filter_by")).select_by_visible_text(filter_by)
        if filter_type in ['i', 'e', "include", "exclude"]:
            Select(driver.find_element_by_id("add_filter_type")).select_by_id("add_filter_" + filter_type[0])
        else:
            Select(driver.find_element_by_id("add_filter_type")).select_by_visible_text = filter_type
        driver.find_element_by_id("add_filter_submit").click()

        filter_el = driver.find_element_by_id("token-input-filter_value_" + 'i' if filter_type in ['i', "is", "include"] else 'e'+  "_dip")
        filter_el.click()
        for val in values:
            filter_el.send_keys(val + Keys.ENTER)

        filter_el.find_element_by_css_selector("input.update_button.update_row_button").click()
        self.wait_until_complete()

    @property
    def overview_lines(self):
        """
        Returns vision breakdown overview in the form of dictionary
        """

        time.sleep(3) ## Sometimes the the overview isn't ready even if progress bar diappears

        lines = self.driver.find_elements_by_xpath(
            "//div[@id='highchart_vis_%s_table']/table/tbody/tr" % self.view_id)

        return dict((
            (
                line.find_element_by_class_name("name").text.strip(),
                line.find_element_by_class_name("value").text.strip() if line.find_elements_by_class_name("value") else None
            )
            for line in lines
        ))

    def get_traffic_overview_line(self, num):
        """
        Return line in the form of tuple of the traffic overview table
        """

        try:
            line = self.driver.find_element_by_xpath(
                "//div[@id='highchart_vis_%s_table']/table/tbody/tr[%d]" % (self.view_id, num))


            return (
                line.find_element_by_class_name("name").text.strip(),
                line.find_element_by_class_name("value").text.strip() )
        except:
            return None

    def wait_until_complete(self):
        # d = driver:
        view = self.view_id
        self.wait.until(lambda d: d.find_element_by_id(
            "progress_" + view).value_of_css_property("display") == 'none')

    def wait_element_by_xpath(self, xpath):
        """Wait for appearance of an element selectable by XPath"""
        return self.wait.until(
            lambda dr: dr.find_element_by_xpath(xpath)
        )

    def wait_element_by_link_text(self, link_text):
        return self.wait.until(
            lambda dr: dr.find_element_by_link_text(link_text)
        )

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException, e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def accept_alert(self):
        try:
            alert = self.driver.switch_to_alert()
            alert.accept()
        except:
            pass

    def create_rotfile_zero(self, rotfile_name='zero'):

        try:
            if self.__create_rotfile_zero_run:
                return
        except:
            pass

        rotfile = 'zero'
        source_port = default_netsource_port
        pipe_name = rotfile + '_pipe'

        self.remove_rotfile(rotfile_name)
        self.create_rotfile(rotfile_name)

        self.get(self.data_pipes_url)

        source_name = 'p' + str(source_port)
        self.create_network_source(source_port, source_name)

        self.create_network_source_to_rotfile_pipe(
            pipe_name, rotfile_name, source_name)

        self.start_pipe(pipe_name)

        self.capture_erffile_to_port(erf_file, source_port)
        time.sleep(3)

        driver = self.driver
        id = driver.find_element_by_xpath(
            "//tr[@class='row_container' and .//input[@value='" + pipe_name + "']]").get_attribute("id")
        self.wait.until(
            lambda d: d.find_element_by_id("output_pkts_" + id).text != "0")

        # self.wait.
        self.stop_pipe(pipe_name)
        # TODO: remove source and pipe

        self.__create_rotfile_zero_run = True

    def get_vision(self):
        return self.get('vision/visualizations')

    def get_managament(self):
        return self.get('admin/launch?script=rh&template=mon-summary')

if __name__ == "__main__":
    # user_traffic = os.environ.get("TRACEFILE")
    # if user_traffic is None:
    #   user_traffic = default_tracefile
    vis = Vision(probe=default_probe)
    vis.get(vis.base_url + "vision/visualizations/view/6")
    vis.check_select("udp")
    vis.check_select("http")
