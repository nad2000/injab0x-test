#!/bin/bash
# Requires: $TARGET_HOST defined (ie, export TARGET_HOST=192.168.xxx.xxx)
#           python--virtualenv, python-pytest and pip installed
#           traffic file STNT_ALIGNED_FIRST_1_SEC.erf in working directory
#           VISION, APPDETECT and RESTRICTED_CMDSlicence Arestricted cmds licence
#
# Optional: $1 - verbosity
#           $SELENIUM_SERVER defined - for if you run the test remotely,
#           (e.g., on appbuild-vm)
#           do:
#               export SELENIUM_SERVER=your.selenium.server:port
#           on your local machine

# Usage
show_help() {
   cat <<EOF
   Requires: Trace file STNT_ALIGNED.erf/tar in working directory
             Licences (should already be installed on probe):
               VISION, APPDETECT and RESTRICTED_CMDS
             \$SELENIUM_SERVER defined (optional)
               for if you run the test remotely, (e.g., on appbuild-vm)
               do:
                 export SELENIUM_SERVER=your.selenium.server:port
               on your local machine

   Usage: ${0##*/} -p TARGET_HOST [-t TRACEFILE ] [-x]

       -h             this help

       -p TARGET_HOST override the host address defined by \$TARGET_HOST.

       -t TRACEFILE   use TRACEFILE for testing.
       ^^^not supported! must be STNT_ALIGNED.tar/erf)^^^

       -x             do not upload tracefile.
       Only use this option if you have previously run this script or if you
       have already uploaded the tracefile to /data/TRACEFILE).

       -r SERVER:PORT override the server defined by \$SELENIUM_SERVER.

       -v             increase verbosity.

   Examples:

       ${0##*/} -p 1.2.3.4
   This will run injab0x-tests on the probe at address 1.2.3.4 using the
   synthetic 'TNT' tracefile synthetic_telstra_nexus.erf which should be in
   the same directory as this script.

       ${0##*/} -p 1.2.3.4 -t ~/STNT_ALIGNED.tar
   Same as above, but indicates that the (compressed) synthetic 'TNT'
   tracefile (STNT_ALIGNED.tar) is located in your home directory.
   The tar will be extracted after copying to the probe.

      ${0##*/} -xr \$HOSTNAME.\$DOMAINNAME:4444
   This will run injab0x-tests on the probe defined by \$TARGET_HOST using
   the synthetic 'TNT' tracefile (as above), but will NOT upload the file.
   The selenium server at \$HOSTNAME.\$DOMAINNAME on port 4444 (default)
   will be used.

EOF
}
TOP='________________________________________________________________________________'
BOT='________________________________________________________________________________
````````````````````````````````````````````````````````````````````````````````'
MID='++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
DIVIDER=$BOT'
'$TOP

# DEFAULTS
DOMAINNAME=ems.endace.com
TRACEFILE=$(ls STNT_ALIGNED* 2>/dev/null)
VERBOSITY=0
UPLOAD_TRACEFILE=true
SELENIUM_PORT=4444
OPTIND=1
while getopts ":hp:t:r:vx" opt; do
    case $opt in
	h)
	    show_help
	    exit 0
	    ;;
	v)
	    VERBOSITY=$(($VERBOSITY+1))
	    ;;
	t)
	    TRACEFILE="$OPTARG"
	    ;;
	p)
	    export TARGET_HOST="$OPTARG"
	    ;;
	x)
	    UPLOAD_TRACEFILE=false
	    ;;
	r)
	    export SELENIUM_SERVER=`echo "$OPTARG" | cut -d: -f1`
	    if [[ $OPTAPRG =~ : ]]; then
		export SELENIUM_PORT=`echo "$OPTARG" | cut -d: -f2`
	    fi
	    ;;
	'?')
	    show_help >&2
	    exit 1
	    ;;
	':')
	    case $OPTARG in
		t)
		    # use the default value
		    ;;
		r)
		    ;&
		p)
		    # assume user has set environment variable TARGET_HOST
		    echo "-p given but no target host specified!" >&2
		    ;&

		*)
		    show_help >&2
	    	    exit 1
		    ;;
	    esac
    esac
done
shift "$((OPTIND-1))" #Shift off the options and optional --
if [ $(($VERBOSITY > 0)) ] && ! [ -z "$@" ]
then
    printf 'Ignoring unrecognised parameters: %s\n' "$@"
fi

# CHECK target host
if [ -z "$TARGET_HOST" ]
then
    echo "Environment variable TARGET_HOST must be set or indicated with -p!" >&2
    show_help; exit 1
fi

# START
echo "$TOP"
echo "\$HOSTNAME:        $HOSTNAME"
echo "\$TARGET_HOST:     ${TARGET_HOST}"
#if ! [ -z "$SELENIUM_SERVER" ]; then
#    export SELENIUM_SERVER=${SELENIUM_SERVER%%:*}
#    export SELENIUM_PORT=${SELENIUM_SERVER##*:}
    if [ -z "SELENIUM_PORT" ] ; then
        SELENIUM_PORT=4444
    fi
    echo "\$SELENIUM_SERVER: ${SELENIUM_SERVER}:${SELENIUM_PORT}"
#fi
################################################################################
echo "$DIVIDER"
################################################################################
# Allow access to probedb and visiondb...
echo "Checking we have remote access to the probe databases..."
IPADDRESS=$(ping -c 1 $HOSTNAME.ems.endace.com | grep -o '\([[:digit:]]\{1,3\}\.\)\{3,3\}[[:digit:]]\{1,3\}' | head -n 1)
if (( $VERBOSITY>0 )); then
    echo "Your ip address:  $IPADDRESS"
    echo "Script to pipe into ssh:"'
if ! [[ $(grep "'$IPADDRESS'" /endace/ui_data/pgsql/pg_hba.conf) =~ "'$IPADDRESS'" ]]; then
    echo "host    all    all    '$IPADDRESS'/32    trust" >> /endace/ui_data/pgsql/pg_hba.conf
fi
if ! [[ $(grep "'$IPADDRESS'" /endace/pgsql/pg_hba.conf) =~ "'$IPADDRESS'" ]]; then
    echo "host    all    all    '$IPADDRESS'/32    trust" >> /endace/pgsql/pg_hba.conf
fi
pkill -HUP postgres'
fi
ssh -o "StrictHostKeyChecking no" root@$TARGET_HOST <<EOF
if ! [[ \$(grep "$IPADDRESS" /endace/ui_data/pgsql/pg_hba.conf) =~ "$IPADDRESS" ]]; then
    echo "host    all    all    $IPADDRESS/32    trust" >> /endace/ui_data/pgsql/pg_hba.conf
fi
if ! [[ \$(grep "$IPADDRESS" /endace/pgsql/pg_hba.conf) =~ "$IPADDRESS" ]]; then
    echo "host    all    all    $IPADDRESS/32    trust" >> /endace/pgsql/pg_hba.conf
fi
pkill -HUP postgres
EOF
echo "... we should now have remot access to the probe databases!"
if $UPLOAD_TRACEFILE; then
    ############################################################################
    echo "$DIVIDER"
    ############################################################################
    if [ "$(ssh -q root@$TARGET_HOST "if [ -f /data/synthetic_telstra_nexus.erf ] ; then echo 1; fi")" != "1" ] ; then
        # copy trace file to the target:
        echo "Loading tracefile $TRACEFILE..."
        if [ "$(ssh -q root@$TARGET_HOST "if [ -f /data/$(basename $TRACEFILE) ] ; then echo 1; fi")" != "1" ] ; then
            if ! [ -f $TRACEFILE ]; then echo "Tracefile $TRACEFILE does not exist!"; exit 1; fi
            rsync -v $TRACEFILE root@${TARGET_HOST}:/data/
        fi
        if [[ ${TRACEFILE##*.} =~ tar|gz ]]; then
            ssh root@$TARGET_HOST "cd /data; tar -xf ${TRACEFILE##*/} && rm -rf ${TRACEFILE##*/} && mv STNT_ALIGNED.erf synthetic_telstra_nexus.erf"
        fi
        ssh root@$TARGET_HOST "cd /data; mv STNT_ALIGNED.erf synthetic_telstra_nexus.erf"
        echo "... done!"
    fi
    echo "... done!"
fi
################################################################################
echo "$DIVIDER"
################################################################################
echo "Buildning test environment..."

if [ -d $HOME/testenv ] ; then
    . $HOME/testenv/bin/activate
else
    virtualenv testenv
    . testenv/bin/activate
fi

pip install -r requirements.txt
################################################################################
echo "$DIVIDER"
################################################################################
echo "Running tests..."
py.test --junit-xml  log_$(date +%FT%T).xml  breakdowns.py  cpd.py capturing.py reporting.py
echo "... done!"
echo "$BOT"
exit 0
