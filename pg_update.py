#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import vision
import os
import time
import pytest
slow = pytest.mark.slow

#import unittest

probe = os.environ.get("TARGET_HOST", "192.168.132.111")
tracefile = "synthetic_telstra_nexus.erf"
rotfile = tracefile[:-4]


@pytest.fixture(scope="session")
def capture_traffic(probe=probe, rotfile=rotfile, replay=False, traffic=tracefile):

    """
    Creates a dag to rotfile pipe,
    runs dagflood and captures.
    """

    cli = vision.Cli(probe=probe, user="root", verbose=True)
    cli.capture_traffic(rotfile=rotfile, replay=replay)

    return cli


def test_pg_update(capture_traffic):
    cli = capture_traffic
    db = vision.MetaDb(probe=probe, rotfile=rotfile)
    row_count = db.query_scalar("SELECT count(*) FROM long_flow_samples_600")
    db_count = db.query_scalar("SELECT count(*) FROM pg_database")
    db.close()

    cli.save_config()
    cli.execute("quit")
    cli.execute("/opt/tms/lib/dbmgr/pg_upgrade.sh")
    # wait until PostgreSQL gets started:
    cli.execute("for i in {1..100} ; do (sudo -u postgres /opt/endace/pgsql/bin/pg_ctl status -D /endace/pgsql | grep -q \"PID:\") && break ; sleep 3; done")
    cli.execute("cli -m config -t \"pm process sshd restart\"")
    cli.close()

    db = vision.MetaDb(probe=probe, rotfile=rotfile)
    assert db.source_exists(rotfile), "Erfstore %s should be created." % rotfile
    assert db_count == db.query_scalar("SELECT count(*) FROM pg_database"), "Expected the same number of data bases: %i" % db_count
    assert row_count == db.query_scalar("SELECT count(*) FROM long_flow_samples_600"), "Expected the same number of samples: %i" % row_count

if __name__ == "__main__":
    cli = capture_traffic()
    test_pg_update(cli)
